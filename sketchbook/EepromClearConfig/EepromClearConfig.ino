// This program clears the entire contents of the EEPROM

#define MY_CORE_ONLY

#include <MySensors.h>

void setup() {
  Serial.begin(MY_BAUD_RATE);
  delay(2000);
  Serial.println("Started clearing. Please wait...");
  for (uint16_t i = 0; i < E2END; i++) {
    hwWriteConfig(i, 0xFF);
  }
  Serial.println("Clearing done.");
}

void loop() {
  // Nothing to do here...
}
