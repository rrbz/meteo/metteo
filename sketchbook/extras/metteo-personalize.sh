#!/bin/bash
# Generate PSK AES key stored in EEPROM for RF encryption between nodes.
# See also MetteoPersonalizer sketch.
# #define MY_AES_KEY 0xFF,0xFF,0xFF,...
# Inspired by Stefano Gottardi <st.gottardi (at) libero.it>
# Source:
#   https://forum.mysensors.org/topic/3854/mysigningatsha204soft-1-5-4-hmac-linux-keygenerator
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# version 2 as published by the Free Software Foundation.

if ! which openssl > /dev/null; then
  echo "openssl is not installed"
  exit -1
fi
if ! which arduino-cli > /dev/null; then
  echo "arduino-cli is not installed"
  exit -1
fi
if ! which picocom > /dev/null; then
  echo "picocom is not installed"
  exit -1
fi

readonly SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
readonly KEYS_FILE="$SCRIPT_DIR/keys.psk"

generateKey() {
  local keyLen=$1
  local key=`openssl rand -hex $keyLen`
  local COUNTER=2
  local MYSENS_KEY="0x${key:0:2}"
  until [ $COUNTER -eq $((2*keyLen)) ]; do
    local STRING=${key:$COUNTER:2}
    local MYSENS_KEY="$MYSENS_KEY,0x$STRING"
    let COUNTER+=2
  done
  echo $MYSENS_KEY
}

personalize() {
  local fqbn=$1
  local port=$2
  cd "$SCRIPT_DIR/.."
  echo "cd `pwd`"
  echo "arduino-cli compile -b $fqbn -u -p $port --build-property build.extra_flags=-DMY_AES_KEY=$AES_KEY MetteoPersonalizer/"
  arduino-cli compile -b $fqbn -u -p $port --build-property build.extra_flags=-DMY_AES_KEY=$AES_KEY MetteoPersonalizer/
  local status=$?
  if [ "$status" -eq 0 ]; then
    # If upload went through show output of personalize sketch.
    echo "picocom -q $port -b 115200 -x 5000 --imap lfcrlf"
    picocom -q $port -b 115200 -x 5000 --imap lfcrlf
  fi
}

find_uarts() {
  local uart1=`ls /dev/ttyUSB*`
  local uart2=`ls /dev/ttyACM*`
  local uart3=`ls /dev/ttyAMA*`
  local uart4=`ls /dev/ttyS*`
  local externalUarts=""
  local internalUarts="`readlink -e /dev/serial[01]`"
  for uart in $uart1 $uart2 $uart3 $uart4 ; do
    if echo $internalUarts | grep -w -q $uart; then
      # Skip internal uarts
      continue
    fi
    local externalUarts="$externalUarts $uart"
  done
  echo "$externalUarts"
}

_keys_firstrun=0
if [ -f "$KEYS_FILE" ]; then
  echo "Found previously generated keys file."
  source "$KEYS_FILE"
else
  echo "Generating new PSK keys."
  readonly AES_KEY=`generateKey 16`
  echo "${AES_KEY@A}" | cut -d ' ' -f 3- > "$KEYS_FILE"
  chmod 600 "$KEYS_FILE"
  _keys_firstrun=1
fi

if [ "$_keys_firstrun" -eq 1 ]; then
  _restart_2mqtt_bridge=0
  if [ "`systemctl is-active 2mqtt.service`" = "active" ]; then
    sudo systemctl stop 2mqtt.service
    _restart_2mqtt_bridge=1
  fi
  # Moteino as serial gateway on GPIO serial port.
  # Use new generated keyfile to personalize the gateway.
  personalize moteino:avr:Moteino /dev/serial0
  if [ "$_restart_2mqtt_bridge" -eq 1 ]; then
    sudo systemctl start 2mqtt.service
  fi
fi

for uart in `find_uarts` ; do
  # Try to personalize any MetteoStation node with the same key as the gateway.
  personalize moteino:avr:MoteinoMEGA $uart
done

cd "$SCRIPT_DIR/.."
echo "To personalize any other metteo node connected to a given serial port execute:"
echo "cd `pwd`"
echo "arduino-cli compile -b moteino:avr:MoteinoMEGA -u -p /dev/ttyACM0 --build-property build.extra_flags=-DMY_AES_KEY=$AES_KEY MetteoPersonalizer/"
exit 0
