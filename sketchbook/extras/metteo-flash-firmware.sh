#!/bin/bash
# Flash the firmware to each connected MySensors node.
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# version 2 as published by the Free Software Foundation.

if ! which arduino-cli > /dev/null; then
  echo "arduino-cli is not installed"
  exit -1
fi
if ! which picocom > /dev/null; then
  echo "picocom is not installed"
  exit -1
fi

readonly SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

flash() {
  local fqbn=$1
  local port=$2
  local sketch=$3
  cd "$SCRIPT_DIR/.."
  echo "cd `pwd`"
  echo "arduino-cli compile -b $fqbn -u -p $port $sketch/"
  arduino-cli compile -b $fqbn -u -p $port $sketch/
  local status=$?
  if [ "$status" -eq 0 ]; then
    # If upload went through show output of the sketch.
    echo "picocom -q $port -b 115200 -x 5000 --imap lfcrlf"
    picocom -q $port -b 115200 -x 5000 --imap lfcrlf
  fi
}

find_uarts() {
  local uart1=`ls /dev/ttyUSB*`
  local uart2=`ls /dev/ttyACM*`
  local uart3=`ls /dev/ttyAMA*`
  local uart4=`ls /dev/ttyS*`
  local externalUarts=""
  local internalUarts="`readlink -e /dev/serial[01]`"
  for uart in $uart1 $uart2 $uart3 $uart4 ; do
    if echo $internalUarts | grep -w -q $uart; then
      # Skip internal uarts
      continue
    fi
    local externalUarts="$externalUarts $uart"
  done
  echo "$externalUarts"
}

_restart_2mqtt_bridge=0
if [ "`systemctl is-active 2mqtt.service`" = "active" ]; then
  sudo systemctl stop 2mqtt.service
  _restart_2mqtt_bridge=1
fi
# Moteino as serial gateway on GPIO serial port.
flash moteino:avr:Moteino /dev/serial0 MetteoGateway
if [ "$_restart_2mqtt_bridge" -eq 1 ]; then
  sudo systemctl start 2mqtt.service
fi

for uart in `find_uarts` ; do
  # Try to flash any MetteoStation node connected via a serial adapter.
  flash moteino:avr:MoteinoMEGA $uart MetteoStation
done

exit 0
