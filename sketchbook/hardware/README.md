For information on installing 3rd party board definitions, see:
 * [Platform specification - Arduino CLI](https://arduino.github.io/arduino-cli/latest/platform-specification/)
 * [Arduino Makefile](https://github.com/sudar/Arduino-Makefile/blob/master/README.md#arduino-arm-boards)
 * [Makefile Example](https://github.com/sudar/Arduino-Makefile/blob/master/examples/MakefileExample/Makefile-3rd_party-board.mk)
