# Metteo Wireless Arduino Weather Station
This folder contains all the files needed to build the wireless Arduino weather station that connects
to the Raspberry Pi MQTT gateway.

The hardware is based upon the Moteino platform from LowPowerLab and the RFM69HW radio from HOPERF.
The software instead uses a variant of the MySensors library.

The default Arduino sketch can be uploaded to the weather station board either by Arduino IDE or the
Arduino Makefile.

## See also:
 * [All about Moteino | LowPowerLab](https://lowpowerlab.com/guide/moteino/)
 * [RFM69/95 & Arduino](https://www.mysensors.org/build/connect_radio#rfm6995-&-arduino)
 * [Arduino Makefile](https://github.com/sudar/Arduino-Makefile)
 * [How to Upload an Arduino Sketch from Linux Terminal](https://www.youtube.com/watch?v=qAM2S27FWAI)

### Wiring
The sensors of the original Davis ISS Transmitter model 6152EU are wired via RJ-25 connectors.

Starting from the left the plugs are assigned to [UV DW-6490](extras/6490_SS.pdf),
[SUN DW-6450](extras/6450_SS_Rev_D.pdf), [RAIN DW-7852](extras/7857_7852_SS.pdf),
[WIND DW-6410](extras/6410_SS.pdf), and TEMP/HUM DW-7346.xxx with following
pinout:
```
/***************************************************************************************************
 * Sensor RJ-25 pinout front view (tab on the bottom, cable goes out on the back side):
 *   Cable colors:
 *    - WHT white   - BLK black   - RED red
 *    - GRN green   - YLW yellow  - BLU blue
 *
 * UV (150mV per UV index):       SUN (1.67mV per W/m²):         RAIN (0.2mm bucket, < 100mm/hr):
 *
 *        .-------- +3V ±10% sw.         .-------- +3V ±10% sw.         .-------- GND
 *        | .------ UV (analog)          | .------ SUN (analog)         | .------ GND
 *        | | .---- GND                  | | .---- GND                  | | .---- SW (open drain)
 *        | | | .-- GND                  | | | .-- GND                  | | | .-- GND
 *        | | | |                        | | | |                        | | | |
 * Pos: 6 5 4 3 2 1               Pos: 6 5 4 3 2 1               Pos: 6 5 4 3 2 1
 *   ### # # # # # ###              ### # # # # # ###              ### # # # # # ###
 *   # #_#_#_#_#_#_# #              # #_#_#_#_#_#_# #              # #_#_#_#_#_#_# #
 *   #    Y G R B    #              #    Y G R B    #              #    Y G R B    #
 *   #    L R E L    #              #    L R E L    #              #    L R E L    #
 *   #    W N D K    #              #    W N D K    #              #    W N D K    #
 *   ###+---------+###              ###+---------+###              ###+---------+###
 *      |_________|                    |_________|                    |_________|
 *
 * WIND (20kΩ pot, 1600 rev/hr = 1mph):   TEMP/HUM (SHT1x):
 *                                             .----------- DAT
 *        .-------- +3V ±10% sw.               | .--------- +3V ±10% sw.
 *        | .------ DIR (analog)               | | .------- GND
 *        | | .---- GND                        | | |
 *        | | | .-- SPEED (open drain)         | | |  
 *        | | | |                              | | |     .- CLK
 * Pos: 6 5 4 3 2 1                       Pos: 6 5 4 3 2 1
 *   ### # # # # # ###                      ### # # # # # ###
 *   # #_#_#_#_#_#_# #                      # #_#_#_#_#_#_# #
 *   #    Y G R B    #                      #  B Y G     W  #
 *   #    L R E L    #                      #  L L R     H  #
 *   #    W N D K    #                      #  U W N     T  #
 *   ###+---------+###                      ###+---------+###
 *      |_________|                            |_________|
 *
 **************************************************************************************************/

```
### Moteino
The Moteino board was used for software development and the final weather station uses a
Moteino Mega variant.
[![Moteino pinout](extras/moteino.jpg)](extras/moteino.png)
[![Moteino MEGA pinout](extras/moteino-mega.jpg)](extras/moteino-mega.png)
