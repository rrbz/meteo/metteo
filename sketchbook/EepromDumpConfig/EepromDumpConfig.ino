// This program print the entire contents of the EEPROM

#define MY_CORE_ONLY

#define NODEMANAGER_DEBUG OFF
#define NODEMANAGER_INTERRUPTS OFF
#define NODEMANAGER_SLEEP OFF
#define NODEMANAGER_RECEIVE OFF
#define NODEMANAGER_DEBUG_VERBOSE OFF
#define NODEMANAGER_POWER_MANAGER OFF
#define NODEMANAGER_CONDITIONAL_REPORT OFF
#define NODEMANAGER_EEPROM OFF
#define NODEMANAGER_TIME OFF
#define NODEMANAGER_RTC OFF
#define NODEMANAGER_SD OFF
#define NODEMANAGER_HOOKING OFF
#define NODEMANAGER_OTA_CONFIGURATION OFF
#define NODEMANAGER_SERIAL_INPUT OFF

#include <MySensors_NodeManager.h>

void setup() {
  enum {
    // 16 bytes at a time
    GROUPING = 16,
  };
  Serial.begin(MY_BAUD_RATE);                    // initalize serial port
  delay(2000);
  Serial.println(F("EEPROM DUMP:"));             // information, notice the F() wrapper
  unsigned addr = 0;                             // used for address in EEPROM
  do {
    char buffer[GROUPING + 3]{ ' ', ' ', '\0' }; // char array for ASCII string
    /* Read 16 characters from the EEprom into our buffer */
    /* ------ Print the address padded with spaces ------ */
    if (addr < 1000) { Serial.print(' '); }      // space padding, char(32) is a space
    if (addr < 100) { Serial.print(' '); }       // space padding
    if (addr < 10) { Serial.print(' '); }        // space padding
    Serial.print(addr, DEC);                     // starting address for line
    Serial.print("  ");                          // seperator, could be replaced with a tab ("\t")
    for (byte i = 0, j = 0; i < GROUPING; i++) {
      byte c = hwReadConfig(addr + i);           // read one byte
      buffer[i + 2] = char(c);                   // stuff the character in our buffer
      /*--- print HEX value ---*/
      if (c < 0x10) { Serial.print('0'); }       // pad HEX value with 0, 0x10 = F0 = 16
      Serial.print(c, HEX);                      // usinh HEX tells Serial how to format the number
      j++;                                       // increment index for serperator
      if (j == 4) {                              // if we have print 4 values
        Serial.print(' ');                       // print a seperator 
        j=0;                                     // set our index to zero to start over
      }
      /*--- Evaluate character ---*/
      if (c < 32) { buffer[i + 2] = '.'; }       // do not print control codes
      if (c == 92) { buffer[i + 2] = '.'; }      // do not print "\"
      if (c > 126) { buffer[i + 2] = '.'; }      // do not print 'extended' characters
    }
    Serial.print(buffer);
    showEepromLayout(addr, GROUPING);
    addr += GROUPING;
  } while (addr < E2END);                        // E2END is the end of the EEPROM 
  Serial.print(F("Read "));
  Serial.print(addr);
  Serial.println(F(" bytes"));
}

void loop() { }                                  // do nothing

void showEepromLayout(unsigned addr, byte grp) {
  const unsigned NM_EE_USR = EEPROM_LOCAL_CONFIG_ADDRESS + EEPROM_USER_START;
  const unsigned NM_EE_CLD = NM_EE_USR + EEPROM_CHILD_OFFSET;
  for (byte i = 0, d = (grp / 2), c = 1; i < grp; i++) {
    const unsigned romAddr = addr + i;
    if ((romAddr % d) == 0) {
      Serial.println();
      Serial.print(F("      "));
    }
    Serial.print('[');
    if (romAddr == EEPROM_LOCAL_CONFIG_ADDRESS) {
      Serial.print(F("LOCALCFG"));
    } else if (romAddr == NM_EE_USR) {
      Serial.print(F("USRSTART"));
    } else if (romAddr == NM_EE_CLD) {
      Serial.print(F("CLDSTART"));
    } else if (hwReadConfig(romAddr) == 0xFF) {
      Serial.print(F("        "));
    } else if ((romAddr == (NM_EE_CLD + c * EEPROM_CHILD_SIZE)) && (hwReadConfig(romAddr) != 0xFF)) {
      if (c < 100) { Serial.print(' '); }
      if (c < 10) { Serial.print(' '); }
      Serial.print(F("CLDDT")); Serial.print(c++);
    } else if (romAddr == EEPROM_NODE_ID_ADDRESS) {
      Serial.print(F("  NODEID"));
    } else if (romAddr == EEPROM_PARENT_NODE_ID_ADDRESS) {
      Serial.print(F("PARENTID"));
    } else if (romAddr == EEPROM_DISTANCE_ADDRESS) {
      Serial.print(F("DISTANCE"));
    } else if ((romAddr >= EEPROM_ROUTES_ADDRESS) && (romAddr < (EEPROM_ROUTES_ADDRESS + SIZE_ROUTES))) {
      Serial.print(F("ROUTETBL"));
    } else if ((romAddr >= EEPROM_CONTROLLER_CONFIG_ADDRESS) && (romAddr < (EEPROM_CONTROLLER_CONFIG_ADDRESS + SIZE_CONTROLLER_CONFIG))) {
      Serial.print(F("CTRLCONF"));
    } else if (romAddr == EEPROM_PERSONALIZATION_CHECKSUM_ADDRESS) {
      Serial.print(F("PERSCKSM"));
    } else if ((romAddr >= EEPROM_FIRMWARE_TYPE_ADDRESS) && (romAddr < (EEPROM_FIRMWARE_TYPE_ADDRESS + SIZE_FIRMWARE_TYPE))) {
      Serial.print(F("  FWTYPE"));
    } else if ((romAddr >= EEPROM_FIRMWARE_VERSION_ADDRESS) && (romAddr < (EEPROM_FIRMWARE_VERSION_ADDRESS + SIZE_FIRMWARE_VERSION))) {
      Serial.print(F("   FWVER"));
    } else if ((romAddr >= EEPROM_FIRMWARE_BLOCKS_ADDRESS) && (romAddr < (EEPROM_FIRMWARE_BLOCKS_ADDRESS + SIZE_FIRMWARE_BLOCKS))) {
      Serial.print(F("FWBLOCKS"));
    } else if ((romAddr >= EEPROM_FIRMWARE_CRC_ADDRESS) && (romAddr < (EEPROM_FIRMWARE_CRC_ADDRESS + SIZE_FIRMWARE_CRC))) {
      Serial.print(F("   FWCRC"));
    } else if ((romAddr >= EEPROM_SIGNING_REQUIREMENT_TABLE_ADDRESS) && (romAddr < (EEPROM_SIGNING_REQUIREMENT_TABLE_ADDRESS + SIZE_SIGNING_REQUIREMENT_TABLE))) {
      Serial.print(F(" SIGNING"));
    } else if ((romAddr >= EEPROM_WHITELIST_REQUIREMENT_TABLE_ADDRESS) && (romAddr < (EEPROM_WHITELIST_REQUIREMENT_TABLE_ADDRESS + SIZE_WHITELIST_REQUIREMENT_TABLE))) {
      Serial.print(F("WHTELIST"));
    } else if ((romAddr >= EEPROM_SIGNING_SOFT_HMAC_KEY_ADDRESS) && (romAddr < (EEPROM_SIGNING_SOFT_HMAC_KEY_ADDRESS + SIZE_SIGNING_SOFT_HMAC_KEY))) {
      Serial.print(F(" HMACKEY"));
    } else if ((romAddr >= EEPROM_SIGNING_SOFT_SERIAL_ADDRESS) && (romAddr < (EEPROM_SIGNING_SOFT_SERIAL_ADDRESS + SIZE_SIGNING_SOFT_SERIAL))) {
      Serial.print(F("  SERIAL"));
    } else if ((romAddr >= EEPROM_RF_ENCRYPTION_AES_KEY_ADDRESS) && (romAddr < (EEPROM_RF_ENCRYPTION_AES_KEY_ADDRESS + SIZE_RF_ENCRYPTION_AES_KEY))) {
      Serial.print(F("RFAESKEY"));
    } else if (romAddr == EEPROM_NODE_LOCK_COUNTER_ADDRESS) {
      Serial.print(F("NDLOCKCN"));
    } else if (romAddr == EEPROM_LOCAL_CONFIG_ADDRESS) {
      Serial.print(F("LOCALCFG"));
    } else {
      Serial.print(F("        "));
    }
    Serial.print(']');
  }
  Serial.println();
}
