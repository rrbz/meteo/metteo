#pragma once

#include <sensors/SensorBattery.h>

struct SensorLiBattery : public SensorBattery {
  
  SensorLiBattery(int8_t pin = A4) : SensorBattery{} {
    _name = "Battery[LI]";
    children.get()->setDescription(_name);
    // Set the expected Vcc when the battery is fully discharged,
    // used to calculate the percentage (default: 2.7).
    // At 3.1V the DFR0579 controller cuts off power.
    setMinVoltage(3.1);
    // Set the expected Vcc when the battery is fully charged, used
    // to calculate the percentage (default: 3.3).
    // At 4.2V the TP4056 controller terminates the charging.
    setMaxVoltage(4.2);
    // Set measurement via external voltage divider on analog pin.
    setBatteryCalibrationFactor(2.0);
    setBatteryInternalVcc(false);
    setBatteryPin(pin);
  }

  void onSetup() override {
    // Set inital sensor reporting and measure interval.
    setReportTimerValue(nodeManager.getReportIntervalSeconds());
    setMeasureTimerValue(nodeManager.getReportIntervalSeconds());
  }

  void onLoop(Child* child) override {
    // Auto-update sensor reporting and measure interval.
    setReportTimerValue(nodeManager.getReportIntervalSeconds());
    setMeasureTimerValue(nodeManager.getReportIntervalSeconds());
    // Calibrate for Vcc @ 10bit against internal voltage reference.
    float vcc = (float)hwCPUVoltage();
    float volts_per_bit = vcc / (pow(2, 10) * 1000);
    setBatteryVoltsPerBit(volts_per_bit);
    // Measure with Vcc as voltage reference.
    nodeManager.setAnalogReference(DEFAULT);
    // Measure the battery via voltage divider.
    float volt = analogRead(_battery_pin) * _battery_volts_per_bit;
    volt = volt * _battery_adj_factor;
    child->setValue(volt);
    if (_send_battery_level && child->valueReadyToSend()) {
      // Calculate the percentage.
      int percentage = ((volt - _battery_min) / (_battery_max - _battery_min)) * 100;
      if (percentage > 100) {
        percentage = 100;
      }
      if (percentage < 0) {
        percentage = 0;
      }
      // Report battery level percentage.
      if (_send_battery_level) {
        sendBatteryLevel(percentage);
      }
      nodeManager.sleepBetweenSend();
    }
  }
};
