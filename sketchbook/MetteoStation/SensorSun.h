#pragma once

class SensorSun : public Sensor {
  using ambient_temperature_t = float();
#if NODEMANAGER_POWER_MANAGER == ON
  PowerManager pm;
  uint32_t _powerOn_ms;
#endif
  ambient_temperature_t* _ambTempDegC;

public:
  // For power managment some pins gate the PSU to the sensors.
  //  - SUN sensor: pin 21 NPN transistor to GND, active HIGH, 50ms
  SensorSun(
    int8_t pin = A6,
#if NODEMANAGER_POWER_MANAGER == ON
    int8_t pinPM = 21, uint16_t startupMS = 50,
#endif
    uint8_t child_id = 0
  ) : Sensor{ pin }
#if NODEMANAGER_POWER_MANAGER == ON
    , pm{ -1, pinPM, startupMS }, _powerOn_ms{ startupMS }
#endif
    , _ambTempDegC{ nullptr }
  {
    _name = "DW-6450[SUN]";
    children.allocateBlocks(1);
    int sensor_id = nodeManager.getAvailableChildId(child_id);
    new Child(this, FLOAT, sensor_id, S_LIGHT_LEVEL, V_LEVEL, _name, "W/m^2");
#if NODEMANAGER_POWER_MANAGER == ON
    setPowerManager(pm);
#endif
  }

  void onSetup() override {
    // Set inital sensor reporting and measure interval.
    setReportTimerValue(nodeManager.getReportIntervalSeconds());
    setMeasureTimerValue(nodeManager.getReportIntervalSeconds());
  }
  
  void onLoop(Child* child) override {
    // Auto-update sensor reporting and measure interval.
    setReportTimerValue(nodeManager.getReportIntervalSeconds());
    setMeasureTimerValue(nodeManager.getReportIntervalSeconds());
    // Calibrate for Vcc @ 10bit against internal voltage reference.
    float vcc = (float)hwCPUVoltage();
    float mvolts_per_bit = vcc / pow(2, 10);
    // Convert the voltage to a sun rad. intensity.
    // Davis spec sheet: 0-3V; 1.67mV per W/m²
    nodeManager.setAnalogReference(DEFAULT);
    float sun = mvolts_per_bit * analogRead(_pin) / 1.67;
    if ((0.0f < sun) && (nullptr != _ambTempDegC)) {
      // Compensate for temperature if different from ref. temp. 25°C:
      // Davis spec sheet: temp. coefficient +0.12% per °C
      //  -0.12% per °C if >25°C
      //  +0.12% per °C if <25°C
      float dTemp = _ambTempDegC() - 25.0f;
      float tempComp = -0.0012f * dTemp;
      sun += sun * tempComp;
    }
    child->setValue(sun);
  }

#if NODEMANAGER_POWER_MANAGER == ON
  uint32_t powerOnDelay() {
    return _powerOn_ms;
  }
#endif

  void setAmbientTemperatureHook(ambient_temperature_t* function_degC) {
    _ambTempDegC = function_degC;
  }
};
