#pragma once

#include <sensors/SensorBMP280.h>

struct SensorBarometer : public SensorBMP280 {

  SensorBarometer() : SensorBMP280{} {
    children.get(1)->setDescription("BMP280[TEMP]");
    children.get(2)->setDescription("BMP280[PRES]");
    children.get(2)->setUnitPrefix("hPa");
    children.get(3)->setDescription("BMP280[FRCT]");
  }

  void onSetup() override {
    // Set inital sensor reporting and measure interval.
    setReportTimerValue(nodeManager.getReportIntervalSeconds());
    setMeasureTimerValue(nodeManager.getReportIntervalSeconds());
    SensorBMP280::onSetup();
  }

  void onLoop(Child* child) override {
    // Auto-update sensor reporting and measure interval.
    setReportTimerValue(nodeManager.getReportIntervalSeconds());
    setMeasureTimerValue(nodeManager.getReportIntervalSeconds());
    SensorBMP280::onLoop(child);
  }
};
