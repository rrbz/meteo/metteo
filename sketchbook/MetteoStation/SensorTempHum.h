#pragma once

#include <SHT1x.h>

class SensorTempHum : public Sensor {
  SHT1x sht1x;
#if NODEMANAGER_POWER_MANAGER == ON
  PowerManager pm;
  uint32_t _powerOn_ms;
#endif
  uint32_t _tBusy_ms;
  float _ambientTemperature;
  
public:
  // For power managment some pins gate the PSU to the sensors.
  //  - TEMP/HUM:   pin 24 NPN transistor to GND, active HIGH, 50ms
  SensorTempHum(
    int8_t pinDAT = 0, int8_t pinCLK = 1,
#if NODEMANAGER_POWER_MANAGER == ON
    int8_t pinPM = 24, uint16_t startupMS = 50,
#endif
    uint8_t child_id = 0
  ) : Sensor{ -1 }, sht1x{ pinDAT, pinCLK }
#if NODEMANAGER_POWER_MANAGER == ON
    , pm{ -1, pinPM, startupMS }, _powerOn_ms{ startupMS }
#endif
    , _tBusy_ms{ 1040 }, _ambientTemperature{ 25.0f } {
    children.allocateBlocks(2);
    int temp_child_id = nodeManager.getAvailableChildId(child_id);
    new Child(this, FLOAT, temp_child_id, S_TEMP, V_TEMP, "DW-7346[TEMP]");
    int hum_child_id = (child_id > 0) ?
      nodeManager.getAvailableChildId(child_id+1) :
      nodeManager.getAvailableChildId(child_id);
    new Child(this, FLOAT, hum_child_id, S_HUM, V_HUM, "DW-7346[HUM]");
#if NODEMANAGER_POWER_MANAGER == ON
    setPowerManager(pm);
#endif
  }

  void onSetup() override {
    // Set inital sensor reporting and measure interval.
    setReportTimerValue(nodeManager.getReportIntervalSeconds());
    setMeasureTimerValue(nodeManager.getReportIntervalSeconds());
  }

  void onLoop(Child* child) override {
    // Auto-update sensor reporting and measure interval.
    setReportTimerValue(nodeManager.getReportIntervalSeconds());
    setMeasureTimerValue(nodeManager.getReportIntervalSeconds());
    if (child->getType() == V_TEMP) {
      _tBusy_ms = millis(); // Measure busy start time.
      float temperature = sht1x.readTemperatureC();
      _ambientTemperature = temperature;
      temperature = nodeManager.celsiusToFahrenheit(temperature);
      child->setValue(temperature);
    } else if (child->getType() == V_HUM) {
      float humidity = sht1x.readHumidity();
      child->setValue(humidity);
      _tBusy_ms = millis() - _tBusy_ms; // Calculate total busy time.
    }
  }

#if NODEMANAGER_POWER_MANAGER == ON
  uint32_t powerOnDelay() {
    return _powerOn_ms;
  }
#endif

  uint32_t busyDelay() {
    // The sensor delays ca. 1s when connected and 3s when not connected.
    return _tBusy_ms;
  }

  float getAmbientTemperature_degC() {
    return _ambientTemperature;
  }
};
