/*
 * The MySensors Arduino library handles the wireless radio link and protocol
 * between your home built sensors/actuators and HA controller of choice.
 * The sensors forms a self healing radio network with optional repeaters. Each
 * repeater and gateway builds a routing tables in EEPROM which keeps track of the
 * network topology allowing messages to be routed to nodes.
 *
 * Created by Henrik Ekblad <henrik.ekblad@mysensors.org>
 * Copyright (C) 2013-2017 Sensnology AB
 * Full contributor list: https://github.com/mysensors/Arduino/graphs/contributors
 *
 * Documentation: http://www.mysensors.org
 * Support Forum: http://forum.mysensors.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 */

/**********************************
 * MySensors node configuration
 */
#include "MySensorsConfig.h"

/***********************************
 * NodeManager configuration
 */

#define NODEMANAGER_DEBUG OFF
#define NODEMANAGER_INTERRUPTS ON
#define NODEMANAGER_SLEEP ON
#define NODEMANAGER_RECEIVE OFF
#define NODEMANAGER_DEBUG_VERBOSE OFF
#define NODEMANAGER_POWER_MANAGER ON
#define NODEMANAGER_CONDITIONAL_REPORT OFF
#define NODEMANAGER_EEPROM OFF
#define NODEMANAGER_TIME OFF
#define NODEMANAGER_RTC ON
#define NODEMANAGER_SD OFF
#define NODEMANAGER_HOOKING OFF
#define NODEMANAGER_OTA_CONFIGURATION OFF
#define NODEMANAGER_SERIAL_INPUT OFF

// Workaround to enable Interrupts on ATMega1284P:
#if defined(__AVR_ATmega1284P__)
#define INTERRUPT_PIN_1 10
#define INTERRUPT_PIN_2 11
#endif

// Import NodeManager library
// (a nodeManager object will be then made available).
#include <MySensors_NodeManager.h>

/***********************************
 * Main sketch with Metteo sensors:
 **********************************/

// Sensors in order as found on Davis Vantage Pro2 ISS:
// UV, SUN, RAIN, WIND, TEMP/HUM
// Additionally also measure: Barometer, Battery, Radio

// Davis DW-6490 UV sensor @ID=1, measuring UV index.
#include "SensorUV.h"
SensorUV sensorUV;

// Davis DW-6450 sun sensor @ID=2, measuring W/m².
#include "SensorSun.h"
SensorSun sensorSun;

// Davis DW-7852 rain sensor @ID=3, measuring mm/h.
#include "SensorRain.h"
SensorRain sensorRain;

// Davis DW-6410 wind sensor @IDs=4&5, measuring km/h & direction.
#include "SensorWind.h"
SensorWind sensorWind;

// Davis DW-7346 temperature & humidity sensor @IDs=6&7,
// measuring deg. celsius & relative humidity in percent.
#include "SensorTempHum.h"
SensorTempHum sensorTempHum;

// Bosch BMP280 pressure sensor @IDs=8&9&10,
// measuring deg. celsius, pressure in hPa + forecast.
#include "SensorBarometer.h"
SensorBarometer sensorBarometer;

// DFRobot Solar Power Manager Micro DFR0579 3.3V OUT @ID=11.
#include "SensorSolarPowerManager.h"
SensorSolarPowerManager sensorSolarPowerManager;

// Internal ADC via voltage divider @ID=201,
// measuring Li-Ion battery in V and charge percentage.
#include "SensorLiBattery.h"
SensorLiBattery sensorLiBattery;

// RFM69 radio RSSI & TX power @ID=202&203, measuring in dBm.
#include "SensorRadio.h"
SensorRadio sensorRadio;

#if NODEMANAGER_POWER_MANAGER == ON
#include "BoardPowerManager.h"
BoardPowerManager bpm;
#endif

enum {
  // Send every hour the presentation of this node.
  T_PRESENTATION = 60/*min*/ * 60/*s*/,
  // Allow max 1%/hr on air radio activity.
  T_IDLE_DUTY = 100 - 1,
};

// When battery is to low, disable reporting and cut power.
void disableSensors() {
  sensorUV.setEnabled(false);
  sensorSun.setEnabled(false);
  sensorRain.setEnabled(false);
  sensorWind.setEnabled(false);
  sensorTempHum.setEnabled(false);
  sensorBarometer.setEnabled(false);
  sensorLiBattery.setEnabled(false);
  sensorRadio.setEnabled(false);
  bpm.powerOff();
}

// Reenable sensors when battery is OK.
void enableSensors() {
  bpm.powerOn();
  sensorUV.setEnabled(true);
  sensorSun.setEnabled(true);
  sensorRain.setEnabled(true);
  sensorWind.setEnabled(true);
  sensorTempHum.setEnabled(true);
  sensorBarometer.setEnabled(true);
  sensorLiBattery.setEnabled(true);
  sensorRadio.setEnabled(true);
}

// Assume initial report interval found empirically.
void setReportInterval(uint32_t wait_ms = 52668UL) {
  uint32_t tSeconds = wait_ms / 1000.0f + 0.5f;
  // Set sensors update rate and sleep duration.
  nodeManager.setSleepSeconds(tSeconds);
  nodeManager.setReportIntervalSeconds(tSeconds);
  // Set report interval for sensors that can not update itself.
  sensorRain.setReportTimerValue(tSeconds);
}

void repeatPresentation() {
  static InternalTimer presentationTimer;
  if (presentationTimer.getMode() == NOT_CONFIGURED) {
    // Setup a timer such that every hour is sent the presentation of this node.
    presentationTimer.setMode(TIME_INTERVAL);
    presentationTimer.setValue(T_PRESENTATION);
    presentationTimer.start();
  }
  // Report presentation only when no interrupt was active, i.e. not equal -1.
  if (nodeManager.getLastInterruptPin() < 0) {
    //Serial.print("pres="); Serial.println(presentationTimer.isOver()?"true":"false");
    if (presentationTimer.isOver()) {
      nodeManager.presentation();
      presentationTimer.start();
    }
  }
}

void before() {
  // Inform controller about used measurement units.
  nodeManager.setSendUnitPrefix(true);
  // Update first sleep end report cycle time.
  setReportInterval();
#if NODEMANAGER_POWER_MANAGER == OFF
  // If power management is disabled assume that the node is using no battery
  // for development purposes so therefore power on all the sensors. 
  // Following pins must be enabled to power the sensors:
  // - 3V3 supply: pin 19 PMOS transistor to VCC, active LOW
  // - UV sensor:  pin 20 NPN transistor to GND, active HIGH, turn on delay 1s
  // - SUN sensor: pin 21 NPN transistor to GND, active HIGH, turn on delay 50ms
  // - TEMP/HUM:   pin 24 NPN transistor to GND, active HIGH, turn on delay 50ms
  pinMode(19, OUTPUT); digitalWrite(19, LOW);
  int8_t gatePins[]{ 20, 21, 24 };
  for (int8_t pin : gatePins) {
    pinMode(pin, OUTPUT); digitalWrite(pin, HIGH);
  }
#endif
#if NODEMANAGER_TIME == ON
  // Sync clock every hour.
  //nodeManager.setSyncTimeAfterInterval(60);
#endif
#if (NODEMANAGER_SLEEP == ON) && (NODEMANAGER_OTA_CONFIGURATION == OFF)
  // Smart sleep makes only sense when battery consumption is not so important.
  // But we need to conserve as much as power as possible and the additional
  // 500ms delay inhibits fast measurements of wind speed and the rain gauge.
  nodeManager.setSmartSleep(false);
#endif
  nodeManager.before();
}

void presentation() {
  // The presentation is repeated also after some given time.
  nodeManager.presentation();
}

void setup() {
  // Enable temperature compensation of the sun sensor.
  sensorSun.setAmbientTemperatureHook(
    []() { return sensorTempHum.getAmbientTemperature_degC(); }
  );
  // Set main node configuration.
  nodeManager.setGetControllerConfig(false);
  nodeManager.setIsMetric(true);
  nodeManager.setup();
}

// TODO:
// There is a bug within NodeManager v1.8 & v1.9-dev when sensor values are due
// to be reported but because some INTx interrupt is happening during report
// timer rollover none of the sensor results gets sent to the gateway.
void loop() {
  // Measure loop time needed to transmit over the radio.
  // The on air time is approximated by taking in account major expensive calls
  // from within the repeated presentation and sensor loop. See also tBusy_ms.
  uint32_t t0_ms = millis();
  // Repeat presentation after some given time.
  repeatPresentation();
  // Do elaborate all the sensor values.
  nodeManager.loop();
  // End of measuring time needed to transmit all sensor values.
  uint32_t t1_ms = millis();
  // Accumulate the busy time of sensors when radio is not sending.
  // TEMP/HUM sensor has variable busy delays depending on if its cable is
  // connected on the port or not. 
  uint32_t tBusy_ms = sensorTempHum.busyDelay();
#if NODEMANAGER_POWER_MANAGER == ON
  // Account also for global BoardPowerManager turn on delay.
  tBusy_ms += bpm.powerOnDelay();
  // The UV sensor is notorious for its long turn on delay at arround 1s.
  tBusy_ms += sensorUV.powerOnDelay();
  tBusy_ms += sensorSun.powerOnDelay();
  tBusy_ms += sensorTempHum.powerOnDelay();
#endif
  // Exclude the case where Node Manager did wake up from an interrupt.
  if ((t1_ms - t0_ms) > tBusy_ms) {
    // Set next sleep duration to maintain 1% TX on air duty cycle.
    uint32_t tWait_ms = (t1_ms - t0_ms - tBusy_ms) * T_IDLE_DUTY;
    //Serial.print(tWait_ms); Serial.println("ms");
    // Update next sleep end report cycle time.
    setReportInterval(tWait_ms);
  }
}

#if NODEMANAGER_RECEIVE == ON
void receive(const MyMessage &message) {
  nodeManager.receive(message);
}
#endif

#if NODEMANAGER_TIME == ON
void receiveTime(unsigned long ts) {
  nodeManager.receiveTime(ts);
}
#endif
