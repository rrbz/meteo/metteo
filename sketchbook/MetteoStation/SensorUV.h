#pragma once

class SensorUV : public Sensor {
#if NODEMANAGER_POWER_MANAGER == ON
  PowerManager pm;
  uint32_t _powerOn_ms;
#endif

public:
  // For power managment some pins gate the PSU to the sensors.
  //  - UV sensor:  pin 20 NPN transistor to GND, active HIGH, 1s
  SensorUV(
    int8_t pin = A7,
#if NODEMANAGER_POWER_MANAGER == ON
    // This is the only sensor that needs a long power on delay.
    // Most likely there is a large builtin capacitor that
    // needs some time till it is charged up.
    int8_t pinPM = 20, uint16_t startupMS = 1000,
#endif
    uint8_t child_id = 0
  ) : Sensor{ pin }
#if NODEMANAGER_POWER_MANAGER == ON
    , pm{ -1, pinPM, startupMS }, _powerOn_ms{ startupMS }
#endif
  {
    _name = "DW-6490[UV]";
    children.allocateBlocks(1);
    int sensor_id = nodeManager.getAvailableChildId(child_id);
    new Child(this, FLOAT, sensor_id, S_UV, V_LEVEL, _name, "UV Index");
#if NODEMANAGER_POWER_MANAGER == ON
    setPowerManager(pm);
#endif
  }

  void onSetup() override {
    // Set inital sensor reporting and measure interval.
    setReportTimerValue(nodeManager.getReportIntervalSeconds());
    setMeasureTimerValue(nodeManager.getReportIntervalSeconds());
  }
  
  void onLoop(Child* child) override {
    // Auto-update sensor reporting and measure interval.
    setReportTimerValue(nodeManager.getReportIntervalSeconds());
    setMeasureTimerValue(nodeManager.getReportIntervalSeconds());
    // Calibrate for Vcc @ 10bit against internal voltage reference.
    float vcc = (float)hwCPUVoltage();
    float volts_per_bit = vcc / (pow(2, 10) * 1000);
    // Convert the voltage to a UV intensity level.
    // Davis spec sheet: 0-2.5V; 150mV per UV index
    nodeManager.setAnalogReference(DEFAULT);
    float uv = volts_per_bit * analogRead(_pin) / 0.15;
    child->setValue(uv);
  }

#if NODEMANAGER_POWER_MANAGER == ON
  uint32_t powerOnDelay() {
    return _powerOn_ms;
  }
#endif
};
