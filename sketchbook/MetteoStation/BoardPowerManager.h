#pragma once

#include <MySensors_NodeManager.h>

// Custom board power manager.
// Reuse existing PowerManager implementation and use it as a standalone object.
// Do not apply this PM on nodeManager.setPowerManager().
class BoardPowerManager : public PowerManager {
  bool hasOnDelay;
  
public:
  // For power managment some pins gate the PSU to the sensors.
  // Other pins are used internally by power manager for each sensor:
  //  - 3V3 supply: pin 19 PMOS transistor to VCC, active LOW
  //  - UV sensor:  pin 20 NPN transistor to GND, active HIGH, 1s
  //  - SUN sensor: pin 21 NPN transistor to GND, active HIGH, 50ms
  //  - TEMP/HUM:   pin 24 NPN transistor to GND, active HIGH, 50ms
  BoardPowerManager(int8_t psuPin = 19, uint32_t wait_ms = 50)
    : PowerManager{ psuPin, -1, wait_ms }, hasOnDelay{ false } { }

  void powerOn() override {
    // Power on the PMOS by turning low the gate pin.
    digitalWrite(_ground_pin, LOW);
    debug(PSTR(LOG_POWER "ON p=%d\n"), _ground_pin);
    // Wait a bit for the devices to settle down.
    if (_wait > 0) {
      hasOnDelay = true;
      wait(_wait);
    }
  }

  void powerOff() override {
    // Power off the PMOS by turning high the gate pin.
    digitalWrite(_ground_pin, HIGH);
    debug(PSTR(LOG_POWER "OFF p=%d\n"), _ground_pin);
  }

  // Get last turn on delay, otherwise returns 0ms.
  // See also: powerOn()
  uint32_t powerOnDelay() {
    bool delay = (hasOnDelay ? _wait : 0);
    hasOnDelay = false;
    return delay;
  }
};
