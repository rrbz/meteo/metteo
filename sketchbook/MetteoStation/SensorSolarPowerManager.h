#pragma once

#include <sensors/SensorDigitalInput.h>

struct SensorSolarPowerManager : public SensorDigitalInput {

	// Using pin D3 on ATmega1284 to detect the state of the DFR0579 Solar Power
	// Manager Micro 3.3V output. Is HIGH if solar panel is charging battery.
	SensorSolarPowerManager(int8_t pin = 3) : SensorDigitalInput{ pin } {
		_name = "DFR0579[SPM]";
		children.get()->setDescription(_name);
		children.get()->setPresentation(S_BINARY);
		children.get()->setType(V_STATUS);
	}

	void onSetup() override {
		// Set inital sensor reporting and measure interval.
		setReportTimerValue(nodeManager.getReportIntervalSeconds());
		setMeasureTimerValue(nodeManager.getReportIntervalSeconds());
	}

	void onLoop(Child* child) override {
		// Auto-update sensor reporting and measure interval.
		setReportTimerValue(nodeManager.getReportIntervalSeconds());
		setMeasureTimerValue(nodeManager.getReportIntervalSeconds());
		SensorDigitalInput::onLoop(child);
	}
};