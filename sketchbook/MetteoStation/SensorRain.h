#pragma once

#include <sensors/SensorRainGauge.h>

struct SensorRain : public SensorRainGauge {
  
  // Using pin 11 (interrupt INT1) on ATmega1284.
  SensorRain(int8_t pin = 11) : SensorRainGauge{ pin } {
    // TODO: Child presentation is reset to V_CUSTOM
    _name = "DW-7852[RAIN]";
    children.get()->setDescription(_name);
    // The sensor accumulates the rain amount in mm for each day.
    children.get()->setUnitPrefix("mm");
    // Setup number of pulses for each unit:
    //  - Using metric system with 1 mm as base unit. 
    //  - Davis spec sheet: 0.2 mm per bucket tips.
    // Therefore 5 tips for each 1 mm of rain.
    setPulseFactor(1.0f / 0.2f);
    // Compensation factor measured trough a dripping water bottle at 544ml
    // which should result in 25.4mm of rain in one hour: 1.27
    setRainCalibrationFactor(1.27f);
    // Setup report timer to tell how often the sensor should be reported.
    setReportIntervalSeconds(nodeManager.getReportIntervalSeconds());
    // Setup measure timer to tell when the sensed value should be reset.
#if NODEMANAGER_TIME == ON
    // Measure the accumulated value since the the beginning of each day. 
    setMeasureTimerMode(EVERY_DAY); // EVERY_MINUTE
#else
    // Measure every 24 hours, assuming we are not running on batteries.
    setMeasureTimerMode(TIME_INTERVAL);
    setMeasureTimerValue(24UL/*hr*/ * 60UL/*min*/ * 60UL/*sec*/);
#endif
  }

  void onSetup() override {
    // Set inital sensor reporting interval.
    setReportTimerValue(nodeManager.getReportIntervalSeconds());
    SensorRainGauge::onSetup();
    // Quirk: Use measure timer to determine the reset interval (daily).
    // So do custom processing instead predefined summing of parent class.
    children.get()->setValueProcessing(NONE);
  }

  void onInterrupt() override {
    // Quirk: Do custom processing by accumulating the childs value here.
    Child* child = children.get();
    child->setValue(child->getValueFloat() + (_rain_adj_factor / _pulse_factor));
  }

  void onLoop(Child* child) override {
    // Update sensor reporting interval. This function may not be called so
    // often, depending on the measure timer interval. So therefore consider to 
    // update it also from the main sketch by calling setReportTimerValue().
    setReportTimerValue(nodeManager.getReportIntervalSeconds());
    if(0.0 < child->getValueDouble()) {
      // Quirk: Make sure that at least once the last encountered value is sent
      //        and then reset it.
      child->sendValue();
      nodeManager.sleepBetweenSend();
      child->setValue(0.0);
    }
  }

  // Change rain gauge calibration factor.
  void setRainCalibrationFactor(float value) {
    _rain_adj_factor = value;
  }

protected:
  float _rain_adj_factor = 1.0f;
};
