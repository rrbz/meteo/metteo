#pragma once

#include <sensors/SensorSignal.h>

class SensorRadio : public SensorSignal {

  constexpr static const char * const LEVEL_UNIT = "dBm";

  struct SensorTxPower : public SensorSignal {

    SensorTxPower(uint8_t child_id = SIGNAL_CHILD_ID + 1)
      : SensorSignal{ child_id } {
      _name = "Radio[TX]";
      children.get()->setUnitPrefix(LEVEL_UNIT);
      children.get()->setDescription(_name);
      setSignalCommand(SR_TX_POWER_LEVEL);
    }

    void onSetup() override {
      // Set inital sensor reporting and measure interval.
      setReportTimerValue(nodeManager.getReportIntervalSeconds());
      setMeasureTimerValue(nodeManager.getReportIntervalSeconds());
    }

    void onLoop(Child* child) override {
      // Auto-update sensor reporting and measure interval.
      setReportTimerValue(nodeManager.getReportIntervalSeconds());
      setMeasureTimerValue(nodeManager.getReportIntervalSeconds());
      SensorSignal::onLoop(child);
    }
  };

  SensorTxPower sensorTxPower;

public:
	SensorRadio() : SensorSignal{}, sensorTxPower{} {
    _name = "Radio[RSSI]";
    children.get()->setUnitPrefix(LEVEL_UNIT);
    children.get()->setDescription(_name);
	};

  void onSetup() override {
    // Set inital sensor reporting and measure interval.
    setReportTimerValue(nodeManager.getReportIntervalSeconds());
    setMeasureTimerValue(nodeManager.getReportIntervalSeconds());
  }

	void onLoop(Child* child) override {
    // Auto-update sensor reporting and measure interval.
    setReportTimerValue(nodeManager.getReportIntervalSeconds());
    setMeasureTimerValue(nodeManager.getReportIntervalSeconds());
    SensorSignal::onLoop(child);
    nodeManager.sleepBetweenSend();
	}

  void setEnabled(bool value, bool just_set = false) {
    SensorSignal::setEnabled(value, just_set);
    sensorTxPower.setEnabled(value, just_set);
  }
};
