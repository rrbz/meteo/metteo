#pragma once

#include <sensors/SensorPulseMeter.h>

class SensorWind : public SensorPulseMeter {
  
  struct SensorWindDir : public Sensor {
    
    SensorWindDir(int8_t pinDir, uint8_t child_id = 0) : Sensor{ pinDir } {
      _name = "DW-6410[WDIR]";
      children.allocateBlocks(1);
      new Child(
        this, FLOAT, nodeManager.getAvailableChildId(child_id),
        S_WIND, V_DIRECTION, _name
      );
    }

    void onSetup() override {
      // Set inital sensor reporting and measure interval.
      setReportTimerValue(nodeManager.getReportIntervalSeconds());
      setMeasureTimerValue(nodeManager.getReportIntervalSeconds());
    }

    void onLoop(Child* child) override {
      // Auto-update sensor reporting and measure interval.
      setReportTimerValue(nodeManager.getReportIntervalSeconds());
      setMeasureTimerValue(nodeManager.getReportIntervalSeconds());
      // Davis spec sheet:
      // Variable resistance 0 - 20k; 10k = south, 180 degrees 
      float val = 360.0f * analogRead(_pin);
      val /= pow(2, 10);
      child->setValue(val);
    }
  };
  
  SensorWindDir sensorWindDir;

  void updateWindSpeedSamplePeriod() {
    uint32_t seconds = nodeManager.getReportIntervalSeconds();
    setReportTimerValue(seconds);
    setMeasureTimerValue(seconds);
    // Dynamically set pulse factor depending on report & measure cycle time:
    //  - Using fomula for mph per sample period in seconds. 
    //  - Davis spec sheet: V = P * (2.25 / T)
    //     Where V = speed in mph,
    //           P = no. of pulses per sample period,
    //           T = sample period in seconds from report & measure timer.
    //  - Using conversion formula:
    //     1mph = 1.609344km/h
    setPulseFactor(1.609344f * 2.25f / seconds);
  }
  
public:
  // Using pin 10 (interrupt INT0) on ATmega1284 for wind speed measurement.
  // Using pin A5 on ATmega1284 for wind direction measurement.
  SensorWind(int8_t pinSpeed = 10, int8_t pinDir = A5)
    : SensorPulseMeter{ pinSpeed }, sensorWindDir{ pinDir } {
    _name = "DW-6410[WIND]";
    children.get()->setFormat(FLOAT);
    children.get()->setPresentation(S_WIND);
    children.get()->setType(V_WIND);
    children.get()->setDescription(_name);
    children.get()->setUnitPrefix("km/h");
    // Reset and override parent class report & measure timer settings.
    setReportIntervalSeconds(nodeManager.getReportIntervalSeconds());
  };

  void onSetup() override {
    // Set inital sensor reporting and measure interval.
    updateWindSpeedSamplePeriod();
    SensorPulseMeter::onSetup();
  }

  void onInterrupt() override {
    // Increment by pulse factor within given timer interval.
    children.get()->setValue(_pulse_factor);
  }

  void onLoop(Child* /*child*/) override {
    // Auto-update sensor reporting and measure interval.
    updateWindSpeedSamplePeriod();
  }

  void setEnabled(bool value, bool just_set = false) {
    // Drop enabled flag also to embedded wind direction sensor.
    SensorPulseMeter::setEnabled(value, just_set);
    sensorWindDir.setEnabled(value, just_set);
  }
};
