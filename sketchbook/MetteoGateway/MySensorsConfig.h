#pragma once

/*
 * The MySensors Arduino library handles the wireless radio link and protocol
 * between your home built sensors/actuators and HA controller of choice.
 * The sensors forms a self healing radio network with optional repeaters. Each
 * repeater and gateway builds a routing tables in EEPROM which keeps track of the
 * network topology allowing messages to be routed to nodes.
 *
 * Created by Henrik Ekblad <henrik.ekblad@mysensors.org>
 * Copyright (C) 2013-2020 Sensnology AB
 * Full contributor list: https://github.com/mysensors/MySensors/graphs/contributors
 *
 * Documentation: http://www.mysensors.org
 * Support Forum: http://forum.mysensors.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 */
/*
 * This file contains settings specific to this node.
 * Please include it in the sketch like so:
 * #include "MySensorsConfig.h"
 * Make sure to include it before
 * #include <MySensors.h>
 */

// Enable debug prints to serial monitor
//#define MY_DEBUG
//#define MY_DEBUG_VERBOSE_CORE
//#define MY_DEBUG_VERBOSE_GATEWAY
//#define MY_DEBUG_VERBOSE_TRANSPORT  
//#define MY_DEBUG_VERBOSE_RFM69
//#define MY_DEBUG_VERBOSE_RFM69_REGISTERS

// Sketch info
#define SKETCH_NAME "MetteoGateway"
#define SKETCH_VERSION "1.0"
// Enable serial gateway
#define MY_GATEWAY_SERIAL

// Flash leds on rx/tx/err
#define MY_DEFAULT_ERR_LED_PIN A2          // Error led pin (D16)
#define MY_DEFAULT_RX_LED_PIN  A0          // Receive led pin (D14)
#define MY_DEFAULT_TX_LED_PIN  LED_BUILTIN // the PCB, on board LED (D9)
// Set blinking period 300ms
#define MY_DEFAULT_LED_BLINK_PERIOD 300
// Inverses the behavior of leds
#define MY_WITH_LEDS_BLINKING_INVERSE

// Enable signal report functionalities (for sketch internal usage)
#define MY_SIGNAL_REPORT_ENABLED
// Radio settings
#define MY_RADIO_RFM69
// ATC on RFM69 works only with the new driver (not compatible with old=default driver)
#define MY_RFM69_NEW_DRIVER
#define MY_RFM69_FREQUENCY RFM69_868MHZ
#define MY_IS_RFM69HW
// Gateways run always without ATC mode
//#define MY_RFM69_ATC_MODE_DISABLED
// Set maximum legal TX power within the EU at 14dBm (25mW)
#define MY_RFM69_TX_POWER_DBM (14u)
// Use encryption key stored in EEPROM set from MetteoPersonalizer sketch
#define MY_RFM69_ENABLE_ENCRYPTION
//#define MY_ENCRYPTION_SIMPLE_PASSWD "MyInsecurePassword"

// Software message signing is broken on MySensors v2.3.2+ (no nonces generated)
//#define MY_SIGNING_SOFT
//#define MY_SIGNING_SOFT_RANDOMSEED_PIN A1
//#define MY_SIGNING_REQUEST_SIGNATURES

// OTA Firmware update not needed since gateway is connected via serial port
//#define MY_OTA_FIRMWARE_FEATURE
//#define OTA_WAIT_PERIOD 300
//#define FIRMWARE_MAX_REQUESTS 2
//#define MY_OTA_RETRY 2
