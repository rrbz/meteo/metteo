/*
 * The MySensors Arduino library handles the wireless radio link and protocol
 * between your home built sensors/actuators and HA controller of choice.
 * The sensors forms a self healing radio network with optional repeaters. Each
 * repeater and gateway builds a routing tables in EEPROM which keeps track of the
 * network topology allowing messages to be routed to nodes.
 *
 * Created by Henrik Ekblad <henrik.ekblad@mysensors.org>
 * Copyright (C) 2013-2019 Sensnology AB
 * Full contributor list: https://github.com/mysensors/MySensors/graphs/contributors
 *
 * Documentation: http://www.mysensors.org
 * Support Forum: http://forum.mysensors.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 *******************************
 *
 * DESCRIPTION
 * The serial gateway prints data received from sensors on the serial link.
 * The gateway accepts input on serial which will be sent out on radio network.
 *
 * The GW code is designed for Moteino ATmega328P Vcc=3.3V @ 16MHz
 *
 * Wire connections (OPTIONAL):
 * - RX/TX/ERR leds need to be connected between VCC (anode) and digital pins with resistor 270-330R in a series
 *
 * LEDs (OPTIONAL):
 * - To use the feature, uncomment any of the MY_DEFAULT_xx_LED_PINs
 * - RX (green) - blink fast on radio message received. In inclusion mode will blink fast only on presentation received
 * - TX (yellow) - blink fast on radio message transmitted. In inclusion mode will blink slowly
 * - ERR (red) - fast blink on error during transmission error or receive crc error
 *
 */

/**********************************
 * MySensors node configuration
 */
#include "MySensorsConfig.h"

// Update this nodes data every 60 seconds
#define T_IDLE (60)

// Power supply monitor
#define PWR_MON_PIN         A7 // Voltage divider to Raspberry Pi 5V supply
#define PWR_MON_REF      3.306 // Reference voltage same as Vcc pin
#define PWR_MON_ADJ      2.009 // Scaling factor to compensate voltage divider
#define PWR_MON_MIN       4.45 // Minimum voltage
#define PWR_MON_MAX       5.25 // Maximum voltage
#define PWR_MON_CID        201 // Monitor child ID
#define PWR_MON_DSC      "PSU"

// Radio receive signal strength
#define SIG_STR_CID        202 // RSSI child ID
#define SIG_STR_DSC     "RSSI"
#define SIG_PWR_CID        203 // TX Power child ID
#define SIG_PWR_DSC    "TXPWR"

// ATmega328P CPU temperature
#define CPU_TMP_CID        204 // Temperature child ID
#define CPU_TMP_DSC      "MCU"

#include <MySensors.h>

void powerSupplyUpdate() {
  static const float voltsPerBit = PWR_MON_REF / pow(2, 10);
  static MyMessage msgVolts(PWR_MON_CID, V_VOLTAGE);
  analogRead(PWR_MON_PIN); // Make dummy reading to flush the ADC mux channel
  float volt = analogRead(PWR_MON_PIN) * voltsPerBit;
  volt *= PWR_MON_ADJ;
  send(msgVolts.set(volt, 2));
  int percentage = ((volt - PWR_MON_MIN) / (PWR_MON_MAX - PWR_MON_MIN)) * 100;
  if (percentage > 100) percentage = 100;
  if (percentage < 0)   percentage = 0;
  sendBatteryLevel(percentage);
}

void signalStrengthUpdate() {
  static MyMessage msgSignal(SIG_STR_CID, V_LEVEL);
  static MyMessage msgPower(SIG_PWR_CID, V_LEVEL);
  int16_t rssi = transportGetSignalReport(SR_RX_RSSI);
  int16_t txPower = transportGetSignalReport(SR_TX_POWER_LEVEL);
  send(msgSignal.set(rssi));
  send(msgPower.set(txPower));
}

void cpuTemperatureUpdate() {
  static MyMessage msgTemps(CPU_TMP_CID, V_TEMP);
  uint8_t oldADMUX = ADMUX;
  ADMUX = (INTERNAL << 6) | 0x08;
  wait(200);
  ADCSRA |= _BV(ADSC);
  while (bit_is_set(ADCSRA,ADSC));
  float temp = (((ADCH << 8) + ADCL) - 324.31) / 1.22;
  ADMUX = oldADMUX;
  send(msgTemps.set(temp, 2));
}

void before() {
  // Enable pullup resistor on unused pins.
  for (uint8_t pin = 2; pin <= 19; pin++) {
    switch (pin) {
      case MY_RFM69_IRQ_PIN:
      case MY_DEFAULT_TX_LED_PIN:
      case MY_RFM69_CS_PIN:
      case PIN_SPI_MOSI:
      case PIN_SPI_MISO:
      case PIN_SPI_SCK:
      case MY_DEFAULT_RX_LED_PIN:
      case MY_DEFAULT_ERR_LED_PIN:
        continue;
      default:
        pinMode(pin, INPUT_PULLUP);
    }
  }
}

void presentation() {
  sendSketchInfo(SKETCH_NAME, SKETCH_VERSION);
  present(PWR_MON_CID, S_MULTIMETER, PWR_MON_DSC);
  present(SIG_STR_CID, S_SOUND, SIG_STR_DSC);
  present(SIG_PWR_CID, S_SOUND, SIG_PWR_DSC);
  present(CPU_TMP_CID, S_TEMP, CPU_TMP_DSC);
  sendHeartbeat();
}

void setup() { }

void loop() {
  static bool init = true;
  if(false == init) {
    presentation();
  } else {
    init = false;
  }
  powerSupplyUpdate();
  signalStrengthUpdate();
  cpuTemperatureUpdate();
  wait(1000UL * T_IDLE);
}
