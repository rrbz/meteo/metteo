# Mockup MQTT for Metteo

The tool `mock-mqtt` allows recording of sensor data from the MySensors network via the default MQTT
broker at port 1883 to a file with given timestamp.

You can keep a long recording process running when using `screen`. Simply type the *screen* command and
launch the script in recording mode with the desired duration. Press **Ctrl+A** then **Ctrl+D** to
detach the screen session. To return to the screen session type `screen -r`.

Furthermore the tool *mock-mqtt* allows to replay a recorded file on a temporary MQTT broker at port
1884 maintaining the same interval of messages as they were recorded.

Run `./mock-mqtt -h` for usage.
