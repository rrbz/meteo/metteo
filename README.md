# METTEO

[![gpl v.3.0 license](https://img.shields.io/badge/code-GPLv3-blue.svg)](https://www.gnu.org/licenses/#GPL)
[![agpl v.3.0 license](https://img.shields.io/badge/code-AGPLv3-blue.svg)](https://www.gnu.org/licenses/#AGPL)
[![Documentation Status](https://readthedocs.org/projects/metteo/badge/?version=latest)](https://metteo.readthedocs.io/en/latest/?badge=latest)

We are Metteo, a group of students with the aim of providing
accurate weather information to the public.

We used to have a weather station, but its control panel broke.
Our goal now is to build a new weather station,
recycling the sensors of the old station, that is hopefully
less expensive to manage but still fully functional.
The sensors we will use can track temperature and humidity
(DW-6830), wind (DW-6410), rain (DW-6463), sun and UV rays (DW-6490).

In order to achieve this goal we have been divided into
three groups: hardware construction, software specialists
and language experts. 
