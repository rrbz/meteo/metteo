#!/bin/bash
# Some inspiration was taken from https://github.com/RPi-Distro/raspi-config

PKGS="openssl picocom"
SRVS=""

is_pi() {
  ARCH=$(dpkg --print-architecture)
  if [ "$ARCH" = "armhf" ] || [ "$ARCH" = "arm64" ] ; then
    return 0
  else
    return 1
  fi
}

CONFIG=/boot/config.txt

if is_pi; then
  CMDLINE=/boot/cmdline.txt
else
  CMDLINE=/proc/cmdline
fi

get_arch() {
  ARCH=$(dpkg --print-architecture)
  if [ "$ARCH" = "armhf" ]; then
    echo "arm"
  else
    echo "$ARCH"
  fi
}

get_serial() {
  if grep -q -E "console=(serial0|ttyAMA0|ttyS0)" $CMDLINE ; then
    echo 0
  else
    echo 1
  fi
}

get_rtc() {
  if grep -q -E "^dtoverlay=i2c-rtc,ds3231" $CONFIG; then
    echo 1
  else
    echo 0
  fi
}

get_2mqtt() {
  if [ -d /opt/apps/2mqtt ]; then
    echo 1
  else
    echo 0
  fi
}

get_myctrl() {
  if [ -d /opt/apps/mycontroller ]; then
    echo 1
  else
    echo 0
  fi
}

get_versions() {
  echo "$(tr -d '\0' < /proc/device-tree/model)"
}

ASK_TO_REBOOT=0
LIB_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
LIB_TITLE="Metteo Configuration Tool"

calc_wt_size() {
  # NOTE: it's tempting to redirect stderr to /dev/null, so supress error
  # output from tput. However in this case, tput detects neither stdout or
  # stderr is a tty and so only gives default 80, 24 values
  WT_HEIGHT=18
  WT_WIDTH=$(tput cols)

  if [ -z "$WT_WIDTH" ] || [ "$WT_WIDTH" -lt 60 ]; then
    WT_WIDTH=80
  fi
  if [ "$WT_WIDTH" -gt 178 ]; then
    WT_WIDTH=120
  fi
  WT_MENU_HEIGHT=$(($WT_HEIGHT-7))
}

_paused_services=""
_do_pause_services() {
  local services="$1"
  for service in $services; do
    # Find all enabled services.
    if [ ! -z `systemctl list-units -a -t service -l --no-legend $service | cut -f1 -d' '` ]; then
      # Find all active services.
      if [ "`systemctl is-active $service`" = "active" ]; then
        systemctl stop $service
        _paused_services="$_paused_services $service"
      fi
    fi
  done
}

_do_rerun_services() {
  local services="$_paused_services"
  for service in $services; do
    systemctl start $service
  done
  _paused_services=""
}

do_sys_update() {
  echo "Executing system update ..."
  _do_pause_services "$1"
  apt-get update &&
  apt-get upgrade &&
  apt autoremove
  _do_rerun_services
  echo " ... system update completed."
}

do_install_requirements() {
  local pkgList="$1"
  for pkg in $pkgList; do
    dpkg -l "$pkg" | grep -q ^ii && continue || apt-get --yes install $pkg
  done
}

do_install_arduino_cli() {
  local ARDUINO_CLI_VER="$1"
  local INSTALLED_VER=$(arduino-cli version 2>/dev/null | perl -pe '($_)=/([0-9]+([.][0-9]+)+)/')
  if [ "$ARDUINO_CLI_VER" != "$INSTALLED_VER" ]; then
    curl -fsSL https://raw.githubusercontent.com/arduino/arduino-cli/master/install.sh | BINDIR=/usr/local/bin sh -s "$ARDUINO_CLI_VER"
    arduino-cli completion bash > /etc/bash_completion.d/arduino-cli.sh
    sudo -H -u "$SUDO_USER" bash -c 'arduino-cli core install arduino:avr'
  fi
}

do_sys_timezone() {
  local currentTz=$(cat /etc/timezone)
  local ipTz=$(curl --fail --silent https://ipapi.co/timezone)
  if [ "$currentTz" != "$ipTz" ]; then
    timedatectl set-timezone "$ipTz"
    dpkg-reconfigure -f noninteractive tzdata
  fi
}

do_sys_rtc() {
  local DEFAULT=--defaultno
  local CURRENT=0
  if [ $(get_rtc) -eq 0 ]; then
    # Always choose no because RTC is not detected directly.
    local DEFAULT=--defaultno
  else
    local CURRENT=1
  fi
  calc_wt_size
  whiptail --title "$LIB_TITLE" --backtitle "$(get_versions)" \
    --yesno "Would you like the DS3231 RTC to be enabled? Current local time is: $(date)" $DEFAULT \
    $WT_HEIGHT $WT_WIDTH $WT_MENU_HEIGHT
  local RET=$?
  if [ $RET -eq $CURRENT ]; then
    ASK_TO_REBOOT=1
  fi
  if [ $RET -eq 0 ]; then
    sed $CONFIG -i -e "s/^#dtoverlay=i2c-rtc,ds3231/dtoverlay=i2c-rtc,ds3231/"
    if ! grep -q -E "dtoverlay=i2c-rtc,ds3231" $CONFIG; then
      printf "dtoverlay=i2c-rtc,ds3231\n" >> $CONFIG
    fi
    local STATUS=enabled
  elif [ $RET -eq 1 ]; then
    sed $CONFIG -i -e "s/^dtoverlay=i2c-rtc,ds3231/#dtoverlay=i2c-rtc,ds3231/"
    local STATUS=disabled
  else
    return $RET
  fi
  whiptail --title "$LIB_TITLE" --backtitle "$(get_versions)" \
    --msgbox "The DS3231 RTC is $STATUS" $WT_HEIGHT $WT_WIDTH $WT_MENU_HEIGHT
}

PKGS="$PKGS python3-paho-mqtt"
SRV_CONSOLE="console-display.service"
SRVS="$SRVS $SRV_CONSOLE"
do_console() {
  mkdir -pv /opt/console
  cd "$LIB_DIR/console"
  if [ "`systemctl is-active $SRV_CONSOLE`" = "active" ]; then
    systemctl stop $SRV_CONSOLE
  fi
  cp -v console-display.py /opt/console/
  cp -v metteo-console /opt/console/
  if [ ! -f /usr/local/bin/metteo-console ]; then
    ln -s /opt/console/metteo-console /usr/local/bin/metteo-console
  fi
  if [ ! -f /etc/systemd/system/$SRV_CONSOLE ]; then
    cat >/etc/systemd/system/$SRV_CONSOLE <<EOL
[Unit]
Description=Console display daemon
Requires=network.target

[Service]
ExecStart=/opt/console/console-display.py

[Install]
WantedBy=multi-user.target
EOL
  fi
  systemctl daemon-reload
  systemctl enable $SRV_CONSOLE
  systemctl start $SRV_CONSOLE
  # Allow poweroff and start via default GPIO3 (SCL1)
  if ! grep -q -E "dtoverlay=gpio-shutdown,gpio_pin=3" $CONFIG; then
    printf "dtoverlay=gpio-shutdown,gpio_pin=3\n" >> $CONFIG
  fi
  if [ -e /etc/init.d/lightdm ]; then
    # Enable HDMI output when using desktop environment.
    sed -i '\\/usr/bin/tvservice --off\d' /etc/rc.local
    tvservice -p
  else
    # Assume HDMI can be powered off when not using desktop environment.
    if ! grep -q -E "/usr/bin/tvservice --off" /etc/rc.local; then
      sed -i -e '$i \/usr/bin/tvservice --off' /etc/rc.local
    fi
  fi
}

do_serial() {
  if [ $(get_serial) -eq 0 ]; then
    # Disable Linux TTY console output.
    echo "File $CMDLINE ..."
    cat $CMDLINE
    echo "... changed to:"
    sed -i $CMDLINE -e "s/console=ttyAMA0,[0-9]\+ //w /dev/stdout"
    sed -i $CMDLINE -e "s/console=serial0,[0-9]\+ //w /dev/stdout"
    ASK_TO_REBOOT=1
  fi
  # Force stable baudrate on Mini UART.
  if grep -q -E "^enable_uart=0" $CONFIG ; then
    sed -i $CONFIG -e "/^enable_uart=0/d"
    ASK_TO_REBOOT=1
  fi
  if ! grep -q -E "^enable_uart=1" $CONFIG ; then
    echo "Updating file $CONFIG adding setting:"
    echo "enable_uart=1" | tee -a $CONFIG
    ASK_TO_REBOOT=1
  fi
  if ! grep -q -E "^dtoverlay=miniuart-bt" $CONFIG ; then
    # Better safe than sorry!
    # Make sure ttyAMA0 is switched to serial0 on any Raspberry Pi.
    echo "Updating file $CONFIG adding setting:"
    echo "dtoverlay=miniuart-bt" | tee -a $CONFIG
    ASK_TO_REBOOT=1
  fi
  # Enable hardware flow control, used for Arduino reset signalling.
  if ! [ -f "/boot/overlays/uart-ctsrts.dtbo" ]; then
    cp -v "${LIB_DIR}/patches/boot/overlays/uart-ctsrts.dtbo" /boot/overlays/
  fi
  if ! grep -q -E "^dtoverlay=uart-ctsrts" $CONFIG ; then
    echo "Updating file $CONFIG adding setting:"
    echo "dtoverlay=uart-ctsrts" | tee -a $CONFIG
    ASK_TO_REBOOT=1
  fi
}

PKGS="$PKGS mosquitto mosquitto-clients"
SRV_MOSQUITTO="mosquitto.service"
SRVS="$SRVS $SRV_MOSQUITTO"
_do_mosquitto_conf() {
  if [ ! -f /etc/mosquitto/conf.d/metteo.conf ]; then
    cat > /etc/mosquitto/conf.d/metteo.conf<<EOL
acl_file /etc/mosquitto/conf.d/metteo.acl
EOL
  fi
  #Allow initally readonly access
  if [ ! -f /etc/mosquitto/conf.d/metteo.acl ]; then
    cat > /etc/mosquitto/conf.d/metteo.acl<<'EOL'
topic read $SYS/#
topic read mysensors-out/#
topic read mysensors-in/#
EOL
    if [ "`systemctl is-active $SRV_MOSQUITTO`" = "active" ]; then
      systemctl restart $SRV_MOSQUITTO
    fi
  fi
}

_do_mosquitto_client_conf() {
  local client="$1"
  local userRandStr="$2"
  local setupDate=`date`
  #Allow readwrite for clients
  if [ -f /etc/mosquitto/conf.d/metteo.acl ]; then
    cat >> /etc/mosquitto/conf.d/metteo.acl<<EOL
# Configuration for $client - $setupDate
user $userRandStr
topic readwrite mysensors-out/#
topic readwrite mysensors-in/#
EOL
    if [ "`systemctl is-active $SRV_MOSQUITTO`" = "active" ]; then
      systemctl restart $SRV_MOSQUITTO
    fi
  fi
}

SRV_2MQTT="2mqtt.service"
SRVS="$SRVS $SRV_2MQTT"
do_2mqtt() {
  local default=
  local ghRepo="mycontroller-org/2mqtt"
  local arch=`get_arch`
  local latestRel=$(curl --fail --silent "https://api.github.com/repos/${ghRepo}/releases/latest" \
    | grep '"tag_name":' | sed -E 's/.*"([^"]+)".*/\1/')
  local latestRelUrl=$(curl --fail --silent "https://api.github.com/repos/${ghRepo}/releases/latest" \
    | grep "\"browser_download_url\": \".*$arch.tar.gz\"" | sed -E 's/.*"([^"]+)".*/\1/')
  if [ -d /opt/apps/2mqtt ]; then
    local default=--defaultno
  fi
  calc_wt_size
  whiptail --title "$LIB_TITLE" --backtitle "$(get_versions)" \
    --yesno "Would you like to install the MyController 2MQTT $latestRel?" $default \
    $WT_HEIGHT $WT_WIDTH $WT_MENU_HEIGHT
  local RET=$?
  if [ $RET -eq 0 ]; then
    # Install release package
    cd "$LIB_DIR"
    if [ -d /opt/apps/2mqtt ]; then
      rm -rf /opt/apps/2mqtt
      if [ "`systemctl is-active $SRV_2MQTT`" = "active" ]; then
        systemctl stop $SRV_2MQTT
      fi
    fi
    wget -O - "$latestRelUrl" | tar xz
    mkdir -p /opt/apps
    mv ./2mqtt-* /opt/apps/2mqtt
    cd /opt/apps/2mqtt/
    chown -R root:root /opt/apps/2mqtt/
    # Setup config and ACLs
    _do_mosquitto_conf
    local userRandStr=`openssl rand -hex 16`
    _do_mosquitto_client_conf 2mqtt $userRandStr
    cp /opt/apps/2mqtt/config.yaml /opt/apps/2mqtt/config.yaml.bak
    cat >/opt/apps/2mqtt/config.yaml <<EOL
logger:
  mode: production
  encoding: console
  level: info

adapters:
  - name: adapter1
    enabled: true
    reconnect_delay: 20s
    provider: mysensors_v2
    source:
      type: serial
      port: /dev/serial0
      baud_rate: 115200
      transmit_pre_delay: 10ms
    mqtt:
      broker: tcp://127.0.0.1:1883
      insecure_skip_verify: false
      username: $userRandStr
      password:
      subscribe: mysensors-in/#
      publish: mysensors-out
      qos: 0
      transmit_pre_delay: 0s
      reconnect_delay: 5s
EOL
    #
    # TODO: Setup mosquitto as readonly for public access
    #
    # Enable service
    if [ ! -f /etc/systemd/system/$SRV_2MQTT ]; then
      cat >/etc/systemd/system/$SRV_2MQTT <<EOL
[Unit]
Description=2mqtt daemon
Requires=network.target
After=mosquitto.service

[Service]
WorkingDirectory=/opt/apps/2mqtt
ExecStart=/opt/apps/2mqtt/2mqtt -config /opt/apps/2mqtt/config.yaml

[Install]
WantedBy=multi-user.target
EOL
    fi
    systemctl daemon-reload
    systemctl enable $SRV_2MQTT
  fi
  if [ "`systemctl is-active $SRV_2MQTT`" != "active" ]; then
    systemctl start $SRV_2MQTT
  fi
}

SRV_MYCTRL="mycontroller2.service"
SRVS="$SRVS $SRV_MYCTRL"
do_myctrl() {
  # Beta release
  #local ghRepo="mycontroller-org/server"
  #local latestRelUrl=$(curl --fail --silent "https://api.github.com/repos/${ghRepo}/releases/latest" \
  #  | grep '"browser_download_url": ".*\.tar.gz"' | sed -E 's/.*"([^"]+)".*/\1/')
  #local latestRelVer=$(echo $latestRelUrl | grep -oP '(?<=-)([0-9]+\.?)+')
  local arch=`get_arch`
  local latestRelUrl="https://github.com/mycontroller-org/server/releases/download/master/mycontroller-server-master-linux-${arch}.tar.gz"
  local latestRelVer=master
  local default=
  cd "$LIB_DIR"
  if [ -d /opt/apps/mycontroller ]; then
    local default=--defaultno
  fi
  calc_wt_size
  whiptail --title "$LIB_TITLE" --backtitle "$(get_versions)" \
    --yesno "Would you like to install MyController.org v2 $latestRelVer?" $default \
    $WT_HEIGHT $WT_WIDTH $WT_MENU_HEIGHT
  local RET=$?
  if [ $RET -eq 0 ]; then
    # Influxdb prerequisite
    if [ ! -f /etc/apt/sources.list.d/influxdb.list ]; then
      local codeName=$(env -i bash -c '. /etc/os-release; echo $VERSION_CODENAME')
      wget -qO- https://repos.influxdata.com/influxdb.key | apt-key add -
      echo "deb https://repos.influxdata.com/debian $codeName stable" | tee /etc/apt/sources.list.d/influxdb.list
      apt-get update
      apt-get install influxdb
      cp -v /etc/influxdb/influxdb.conf /etc/influxdb/influxdb.conf.bak
      cd /etc/influxdb/
      patch < "$LIB_DIR/patches/etc/influxdb/influxdb.conf.patch"
      cd "$LIB_DIR"
      systemctl restart influxdb.service
    fi
    # Install MyController v2
    if [ $(id -u mycontroller 2>/dev/null | wc -l) -eq 0 ]; then
      # Create system user for MyController.org interface
      useradd -s /usr/sbin/nologin -r -M -d /opt/apps/mycontroller mycontroller
    fi
    if [ -d /opt/apps/mycontroller ]; then
      systemctl stop $SRV_MYCTRL 2>/dev/null
      systemctl disable $SRV_MYCTRL 2>/dev/null
      rm -r /opt/apps/mycontroller
    fi
    wget -O - $latestRelUrl | tar xz
    mv ./mycontroller-server-* /opt/apps/mycontroller
    mkdir -p /opt/apps/mycontroller/mc_home/secure_share
    mkdir -p /opt/apps/mycontroller/mc_home/insecure_share
    cp -v /opt/apps/mycontroller/mycontroller.yaml /opt/apps/mycontroller/mycontroller.yaml.bak
    local secretKey=`openssl rand -hex 16`
    cat >/opt/apps/mycontroller/mycontroller.yaml <<EOL
secret: $secretKey

analytics:
  enabled: false

web:
  web_directory: web_console
  enable_profiling: false
  http:
    enabled: true
    bind_address: "0.0.0.0"
    port: 8080
  https_ssl:
    enabled: false
    bind_address: "0.0.0.0"
    port: 8443
    cert_dir: mc_home/certs/https_ssl
  https_acme:
    enabled: false
    bind_address: "0.0.0.0"
    port: 9443
    cache_dir: mc_home/certs/https_acme
    acme_directory:
    email: hello@example.com
    domains: ["mycontroller.example.com"]

logger:
  mode: record_all
  encoding: console
  level:
    core: info
    web_handler: info
    storage: info
    metric: warn

directories:
  data: mc_home/data
  logs: mc_home/logs
  tmp: mc_home/tmp
  secure_share: mc_home/secure_share
  insecure_share: mc_home/insecure_share

bus:
  type: embedded
  topic_prefix: mc_server
  server_url: nats://127.0.0.1:4222
  tls_insecure_skip_verify: false
  connection_timeout: 10s

gateway:
  disabled: false
  types: []
  ids: []
  labels:
    location: server

handler:
  disabled: false
  types: []
  ids: []
  labels:
    location: server

database:
  storage:
    type: memory
    dump_enabled: true
    dump_interval: 10m
    dump_dir: "memory_db"
    dump_format: ["yaml"]
    load_format: "yaml"

  metric:
    disabled: false
    type: influxdb
    uri: http://127.0.0.1:8086
    token:
    username:
    password:
    organization_name:
    bucket_name: mycontroller
    batch_size:
    flush_interval: 1s
    query_client_version:
EOL
    # Setup MQTT ACL
    _do_mosquitto_conf
    local userRandStr=`openssl rand -hex 16`
    _do_mosquitto_client_conf MyController $userRandStr
    if [ ! -f /opt/apps/mycontroller/mc_home/data/storage/memory_db/yaml/gateway__1.yaml ]; then
      mkdir -p /opt/apps/mycontroller/mc_home/data/storage/memory_db/yaml
      cat >/opt/apps/mycontroller/mc_home/data/storage/memory_db/yaml/gateway__1.yaml <<EOL
- id: MySensors
  description: MySensors Serial Gateway 2MQTT Bridge
  enabled: true
  reconnectdelay: 30s
  queuefailedmessage: false
  provider:
    enableInternalMessageAck: true
    protocol:
      broker: tcp://127.0.0.1:1883
      publish: mysensors-in
      qos: 0
      subscribe: mysensors-out/#
      transmitPreDelay: 10ms
      type: mqtt
      username: $userRandStr
    retryCount: 3
    timeout: 500ms
    type: mysensors_v2
  messagelogger:
    type: none
  labels:
    location: server
  others: {}
  state:
    status: up
    message: Started successfully
    since: 2021-08-08T08:53:45.160326807+02:00
  modifiedon: 0001-01-01T00:00:00Z
  lasttransaction: 0001-01-01T00:00:00Z
EOL
    fi
    chown -R mycontroller:mycontroller /opt/apps/mycontroller/
    if [ ! -f /etc/systemd/system/$SRV_MYCTRL ]; then
      cat >/etc/systemd/system/$SRV_MYCTRL <<EOL
[Unit]
Description=MyController.org v2 daemon
Requires=network.target
After=influxdb.service

[Service]
User=mycontroller
Group=mycontroller
WorkingDirectory=/opt/apps/mycontroller
ExecStart=/opt/apps/mycontroller/mycontroller-server -config /opt/apps/mycontroller/mycontroller.yaml

[Install]
WantedBy=multi-user.target
EOL
    fi
    systemctl daemon-reload
    systemctl enable $SRV_MYCTRL
    systemctl start $SRV_MYCTRL
  fi
  if [ $(get_myctrl) -eq 1 ]; then
    local INSTALL="installed"
  else
    local INSTALL="not installed"
  fi
  local STATUS=$(systemctl is-active $SRV_MYCTRL)
  local IP=$(hostname -I | awk '{print $1}')
  calc_wt_size
  whiptail --title "$LIB_TITLE" --backtitle "$(get_versions)" \
    --msgbox "MyController.org v2 $latestRelVer is $INSTALL and the service is $STATUS. The admin panel is available at https://${IP}:8080" \
    $WT_HEIGHT $WT_WIDTH $WT_MENU_HEIGHT
}

_reboot_fw_info() {
  calc_wt_size
  whiptail --title "$LIB_TITLE" --backtitle "$(get_versions)" \
    --msgbox "A reboot is needed before serial ports are available for uploading." \
    $WT_HEIGHT $WT_WIDTH $WT_MENU_HEIGHT
}

do_personalize() {
  if [ $ASK_TO_REBOOT -eq 1 ]; then
    # Serial ports are available only after reboot.
    _reboot_fw_info
    return
  fi
  local default=--defaultno
  if [ ! -f "$LIB_DIR/sketchbook/extras/keys.psk" ]; then
    local default=
  fi
  calc_wt_size
  whiptail --title "$LIB_TITLE" --backtitle "$(get_versions)" \
    --yesno "Would you like to personalize the security settings on all MySensors nodes connected via each serial port?" \
    $default $WT_HEIGHT $WT_WIDTH $WT_MENU_HEIGHT
  local RET=$?
  if [ $RET -eq 0 ]; then
    cd "$LIB_DIR/sketchbook/extras"
    sudo -H -u "$SUDO_USER" bash -c "$LIB_DIR/sketchbook/extras/metteo-personalize.sh"
    do_firmware
  fi
}

do_firmware() {
  if [ $ASK_TO_REBOOT -eq 1 ]; then
    # Serial ports are available only after reboot.
    _reboot_fw_info
    return
  fi
  if [ ! -f "$LIB_DIR/sketchbook/extras/keys.psk" ]; then
    calc_wt_size
    whiptail --title "$LIB_TITLE" --backtitle "$(get_versions)" \
      --msgbox "MySensors node security setup must be run before uploading firmware." \
      $WT_HEIGHT $WT_WIDTH $WT_MENU_HEIGHT
    return
  fi
  local default=
  calc_wt_size
  whiptail --title "$LIB_TITLE" --backtitle "$(get_versions)" \
    --yesno "Would you like to flash the Metteo firmware to all MySensors nodes connected via each serial port?" \
    $default $WT_HEIGHT $WT_WIDTH $WT_MENU_HEIGHT
  local RET=$?
  if [ $RET -eq 0 ]; then
    cd "$LIB_DIR/sketchbook/extras"
    sudo -H -u "$SUDO_USER" bash -c "$LIB_DIR/sketchbook/extras/metteo-flash-firmware.sh"
  fi
}

do_finish() {
  if [ $ASK_TO_REBOOT -eq 1 ]; then
    calc_wt_size
    whiptail --title "$LIB_TITLE" --backtitle "$(get_versions)" \
      --yesno "Would you like to reboot now?" $WT_HEIGHT $WT_WIDTH $WT_MENU_HEIGHT
    if [ $? -eq 0 ]; then # yes
      sync
      reboot
    fi
  fi
  exit 0
}

do_menu() {
  while true; do
    calc_wt_size
    FUN=$(whiptail --title "$LIB_TITLE" --backtitle "$(get_versions)" \
      --menu "Setup Options" $WT_HEIGHT $WT_WIDTH $WT_MENU_HEIGHT \
      --cancel-button Finish --ok-button Select \
      "1 RTC setup                " "Change the Real Time Clock used by the system" \
      "2 2MQTT setup              " "Configure the MySensors serial gateway to MQTT bridge" \
      "3 MyController.org setup   " "Install or upgrade the MySensors controller interface" \
      "4 MySensors node security  " "Setup the security settings of each MySensors node" \
      "5 MySensors node firmware  " "Flash the firmware of gateway and weather station nodes" \
      3>&1 1>&2 2>&3)
    local RET=$?
    if [ $RET -eq 1 ]; then
      do_finish
    elif [ $RET -eq 0 ]; then
      case "$FUN" in
        1\ *) do_sys_rtc ;;
        2\ *) do_2mqtt ;;
        3\ *) do_myctrl ;;
        4\ *) do_personalize ;;
        5\ *) do_firmware ;;
        *) whiptail --title "$LIB_TITLE" --backtitle "$(get_versions)" \
             --msgbox "Programmer error: unrecognized option" $WT_HEIGHT $WT_WIDTH $WT_MENU_HEIGHT ;;
        esac || whiptail --title "$LIB_TITLE" --backtitle "$(get_versions)" \
             --msgbox "There was an error running option $FUN" $WT_HEIGHT $WT_WIDTH $WT_MENU_HEIGHT
    else
      exit 1
    fi
  done
}
