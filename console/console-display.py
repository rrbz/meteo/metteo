#!/usr/bin/env python3
from re import findall
from sys import exit
from time import sleep
from os.path import basename
from subprocess import run, PIPE
from signal import signal, SIGTERM
import RPi.GPIO as GPIO
import paho.mqtt.client as mqtt

# HD44780 character display configuration
LCD_RS    =  5  # GPIO pin  5, board pin 29
LCD_EN    =  6  # GPIO pin  6, board pin 31
LCD_DATA4 = 21  # GPIO pin 21, board pin 40
LCD_DATA5 = 26  # GPIO pin 26, board pin 37
LCD_DATA6 = 19  # GPIO pin 19, board pin 35
LCD_DATA7 = 13  # GPIO pin 17, board pin 33

LCD_WIDTH  = 16    # Characters per row
LCD_LINE_1 = 0x80  # Address first row
LCD_LINE_2 = 0xC0  # Address seconds row

LCD_DAT  = GPIO.HIGH  # RS data
LCD_CMD  = GPIO.LOW   # RS command
EN_DELAY = 0.0005     # EN pulse delay @ 500µs

LCD_CHAR_MAP = {
  '°': '\xDF',
}

# Main MQTT topic prefix.
TOPIC = "mysensors-out/"
SENSORS = {
  # "NODE_ID/SENSOR_ID/COMMAND": ("TITLE", "UNIT")
  "0/201/1":      ("RPi Vin:",        "V"),
  "0/202/1":      ("Console RSSI:",   "dBm"),
  "0/203/1":      ("Console TX-PWR:", "dBm"),
  "0/204/1":      ("Gateway Temp.:",  "°C"),
  "42/1/1":       ("UV index:",       ""),
  "42/2/1":       ("SUN radiation:",  "W/m2"),
  "42/3/1":       ("Rain gauge:",     "mm/h"),
  "42/4/1":       ("Wind speed:",     "km/h"),
  "42/5/1":       ("Wind direction:", "°"),
  "42/6/1":       ("Ambient temp.:",  "°C"),
  "42/7/1":       ("Ambient hum.:",   "%"),
  "42/8/1":       ("Station temp.:",  "°C"),
  "42/9/1":       ("Air pressure:",   "hPa"),
  "42/10/1":      ("Forecast:",       ""),
  "42/201/1":     ("Station batt.:",  "V"),
  "42/202/1":     ("Station RSSI:",   "dBm"),
  "42/255/3/0/0": ("Station charge:", "%"),

}

samples = {}

def gpio_init():
  if as_daemon:
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(LCD_EN, GPIO.OUT)
    GPIO.setup(LCD_RS, GPIO.OUT)
    GPIO.setup(LCD_DATA4, GPIO.OUT)
    GPIO.setup(LCD_DATA5, GPIO.OUT)
    GPIO.setup(LCD_DATA6, GPIO.OUT)
    GPIO.setup(LCD_DATA7, GPIO.OUT)

def lcd_send_byte(bits, mode):
  GPIO.output(LCD_RS, mode)
  # First nibble
  GPIO.output(LCD_DATA4, GPIO.LOW)
  GPIO.output(LCD_DATA5, GPIO.LOW)
  GPIO.output(LCD_DATA6, GPIO.LOW)
  GPIO.output(LCD_DATA7, GPIO.LOW)
  if bits & 0x10 == 0x10:
    GPIO.output(LCD_DATA4, GPIO.HIGH)
  if bits & 0x20 == 0x20:
    GPIO.output(LCD_DATA5, GPIO.HIGH)
  if bits & 0x40 == 0x40:
    GPIO.output(LCD_DATA6, GPIO.HIGH)
  if bits & 0x80 == 0x80:
    GPIO.output(LCD_DATA7, GPIO.HIGH)
  sleep(EN_DELAY)
  GPIO.output(LCD_EN, GPIO.HIGH)
  sleep(EN_DELAY)
  GPIO.output(LCD_EN, GPIO.LOW)
  sleep(EN_DELAY)
  # Second nibble
  GPIO.output(LCD_DATA4, GPIO.LOW)
  GPIO.output(LCD_DATA5, GPIO.LOW)
  GPIO.output(LCD_DATA6, GPIO.LOW)
  GPIO.output(LCD_DATA7, GPIO.LOW)
  if bits&0x01==0x01:
    GPIO.output(LCD_DATA4, GPIO.HIGH)
  if bits&0x02==0x02:
    GPIO.output(LCD_DATA5, GPIO.HIGH)
  if bits&0x04==0x04:
    GPIO.output(LCD_DATA6, GPIO.HIGH)
  if bits&0x08==0x08:
    GPIO.output(LCD_DATA7, GPIO.HIGH)
  sleep(EN_DELAY)
  GPIO.output(LCD_EN, GPIO.HIGH)
  sleep(EN_DELAY)
  GPIO.output(LCD_EN, GPIO.LOW)
  sleep(EN_DELAY)

def lcd_init():
  if as_daemon:
    lcd_send_byte(0x33, LCD_CMD)
    lcd_send_byte(0x32, LCD_CMD)
    lcd_send_byte(0x28, LCD_CMD)
    lcd_send_byte(0x0C, LCD_CMD)
    lcd_send_byte(0x06, LCD_CMD)
    lcd_send_byte(0x01, LCD_CMD)

def lcd_message(message):
  if as_daemon:
    message = message.ljust(LCD_WIDTH," ")
    for i in range(LCD_WIDTH):
      lcd_char = message[i]
      for key, value in LCD_CHAR_MAP.items():
        if lcd_char.find(key) != -1:
          lcd_char = value
      lcd_send_byte(ord(lcd_char), LCD_DAT)
  else:
    print(message)

def lcd_row(row_address):
  if as_daemon:
    lcd_send_byte(row_address, LCD_CMD)

def get_ip():
  ips = run(["hostname", "-I"], stdout=PIPE).stdout.decode("utf-8").split(' ')
  ip = ips[0].replace('\n', '')
  if len(ip) == 0:
    ip = "not configured"
  return ip

def get_rpi_temp():
  meas_temps = run(["vcgencmd", "measure_temp"], stdout=PIPE).stdout.decode("utf-8")
  temps = findall("\d+\.\d+", meas_temps)
  return temps[0] + " °C"

def terminate(signum, frame):
  if as_daemon:
    lcd_init()
    GPIO.cleanup()
  client.loop_stop()
  exit(0)

def mqtt_receive(client, userdata, message):
  for id in SENSORS:
    if message.topic.find(TOPIC + id) != -1:
      samples[id] = message.payload.decode("utf-8")

def main(is_daemon = True, client_id = basename(__file__)):
  global client
  global as_daemon
  as_daemon = is_daemon
  mqtt_host = "127.0.0.1"
  try:
    signal(SIGTERM, terminate)
    # Setup GPIOs
    gpio_init()
    # Setup MQTT subscription
    print("Connecting to " + mqtt_host + " as client " + client_id)
    client = mqtt.Client(client_id)
    while True:
      try:
        client.connect(mqtt_host)
      except ConnectionRefusedError:
        print("Connection failed to " + mqtt_host)
        sleep(1)
      else:
        break
    print("Connected to " + mqtt_host)
    client.loop_start()
    client.subscribe("#")
    client.on_message=mqtt_receive
    # Start updating the display
    while True:
      lcd_init()
      # Start writing on first line the first view:
      ip_address = get_ip()
      lcd_message("RPi IP address:")
      lcd_row(LCD_LINE_2)
      lcd_message(ip_address)
      sleep(4)
      lcd_row(LCD_LINE_1)
      # Start writing second view:
      rpi_temp = get_rpi_temp()
      lcd_message("RPi CPU temp.:")
      lcd_row(LCD_LINE_2)
      lcd_message(rpi_temp)
      sleep(4)
      lcd_row(LCD_LINE_1)
      # Start writing any further view:
      views = { SENSORS[id][0]: val + ' ' + SENSORS[id][1] for id, val in samples.items() }
      for title, value in views.items():
        lcd_message(title)
        lcd_row(LCD_LINE_2)
        lcd_message(value)
        sleep(4)
        lcd_row(LCD_LINE_1)
  except KeyboardInterrupt:
    pass
  terminate(None, None)

if __name__ == '__main__':
  main()
