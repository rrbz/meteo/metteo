# Metteo Console
Shows via a HD44780 character display the current state of the MySensors Gateway and the software running on the Raspberry Pi.

See also:
 - [Raspberry Pi LCD Display: 16x2 Characters Display (HD44780)](https://tutorials-raspberrypi.com/raspberry-pi-lcd-display-16x2-characters-display-hd44780/)
