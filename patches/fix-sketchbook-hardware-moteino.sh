#!/bin/bash
SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
# Execute all patches against Moteino hardware definition
cd "$SCRIPT_DIR/../sketchbook/hardware/moteino/avr/"
pwd
test -f avrdude.conf && rm -v avrdude.conf
patch < "$SCRIPT_DIR/sketchbook/hardware/moteino/avr/programmers.txt.patch"
patch < "$SCRIPT_DIR/sketchbook/hardware/moteino/avr/boards.txt.patch"
patch < "$SCRIPT_DIR/sketchbook/hardware/moteino/avr/platform.txt.patch"
cp -v "$SCRIPT_DIR/sketchbook/hardware/moteino/avr/bootloaders/DualOptiboot_V5.0_atmega1284p_8mhz_38400baud.hex" "$SCRIPT_DIR/../sketchbook/hardware/moteino/avr/bootloaders/"
exit 0
