#!/bin/bash
SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)
# Enable Linux sysfs GPIOs as ISP programmer.
# Run any programm using the linuxgpio programmer as root user.
[ -d /root/ ] && sudo cp -v "$SCRIPT_DIR/sketchbook/hardware/.avrduderc" /root/
exit 0
