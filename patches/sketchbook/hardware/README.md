# Patches for Arduino 3rd party Board definitions

For information on installing 3rd party board definitions, see:
 * [Arduino IDE 1.5 3rd party Hardware specification](https://github.com/arduino/Arduino/wiki/Arduino-IDE-1.5-3rd-party-Hardware-specification)
 * [Arduino Makefile](https://github.com/sudar/Arduino-Makefile/blob/master/README.md#arduino-arm-boards)
 * [Makefile Example](https://github.com/sudar/Arduino-Makefile/blob/master/examples/MakefileExample/Makefile-3rd_party-board.mk)

## Linux GPIO as ISP (Raspberry Pi)
Wire according to the following diagram the Raspberry Pi and the target microcontroller and execute avrdude.

[![Raspberry Pi ISP pinout](linuxgpio-rpi.jpg)](linuxgpio-rpi.jpg)

The **.avrduderc** file needs to be copied to the /root folder and avrdude must be run as root user.
