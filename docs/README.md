# Metteo docs

These are the sources of the documentation
available at [metteo.readthedocs.io](https://metteo.readthedocs.io).

It is written in reStructuredText (RST) and is built by sphinx.

To compile it you'll need to run

```
pip3 install sphinx sphinx_rtd_theme
```

or

```
python3 -m pip install sphinx sphinx_rtd_theme
```

and then 

```
make html
```

If you need to edit it, you can run the `autoregen.sh` file in background
to automatically rebuild the project everytime a file is modified.
