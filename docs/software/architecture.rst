Architecture
============

WIP

The sensors are connected to a board which is alimentated by
a solar panel and a battery. This small board sends
via radio (FIXME: FREQUENCY? MODEL? SPECS?) the informations
to the RaspberryPi with `MySensors Gateway <https://www.mysensors.org/>`_
which broadcasts them via `MQTT <https://en.wikipedia.org/wiki/MQTT>`_ protocol.

A daemon of the `Django <https://djangoproject.com>`_ application listens to
MQTT and, when a new data is recived, is directly pushed into the database.

Finally, the Django application provides a web interface that displays the
real-time data and the archive.
