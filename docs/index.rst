.. Metteo documentation master file, created by
   sphinx-quickstart on Tue Feb  4 11:03:40 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Metteo's documentation!
==================================

We are Metteo, a group of students with the aim of providing
accurate weather information to the public.

We used to have a weather station, but its control panel broke.
Our goal now is to build a new weather station,
recycling the sensors of the old station, that is hopefully
less expensive to manage but still fully functional.
The sensors we will use can track temperature and humidity
(DW-6830), wind (DW-6410), rain (DW-6463), sun and UV rays (DW-6490).

In order to achieve this goal we have been divided into
three groups: hardware construction, software specialists
and language experts.

This documentation is written to allow anyone else to reproduce
this installation or, even better, enhance this (add other compatible
senors, ecc...).


.. toctree::
   hardware/index.rst
   software/index.rst
   authors.rst
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
