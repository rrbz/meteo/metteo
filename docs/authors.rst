Authors
=======

TBD: better explain what everybody done

This project was started within a school activity and was mainly developed by

`Julian Sanin <j54n1n+gitlab@gmail.com>`_ and
`Marco Marinello <me@marcomarinello.it>`_


The following students were involved:

- Giacomo Bonato
- Alessio Dalpiaz
- Gioele Dametto
- Matteo Girelli
- Michele Lovino
- Matteo Moretto
- Anna Nardello
- Davide Romano
- Stevan Strauss De Castro
