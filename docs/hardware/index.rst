Hardware
========

For programming any boards, you need to install the Moteino package in your
Arduino IDE – this includes definitions for all Moteino boards, MightyHat, etc. 
First add the "Moteino core json definition URL" to your Board Manager.
https://gitlab.com/rrbz/meteo/metteo/-/tree/master/sketchbook/hardware/moteino/avr/variants/MoteinoMEGA

Node Menager has the purpose to periodically perform the main function of the
sensor allowing to interact with the sensor and also to configure it remotely.
In our specific case in the configuration list the "NODEMANAGER_POWER_MANAGER"
is in on mode.
https://github.com/mysensors/NodeManager

Throught the use of this links it is possible to acess every single sensor:
https://gitlab.com/rrbz/meteo/metteo/-/blob/master/sketchbook/extras/6410_SS.pdf
https://gitlab.com/rrbz/meteo/metteo/-/blob/master/sketchbook/extras/6450_SS_Rev_D.pdf
https://gitlab.com/rrbz/meteo/metteo/-/blob/master/sketchbook/extras/6490_SS.pdf
https://gitlab.com/rrbz/meteo/metteo/-/blob/master/sketchbook/extras/7857-7852_SS.pdf