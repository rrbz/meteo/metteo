#!/bin/bash
# This script rebuilds automatically the html when a file
# is changed. Intended to be executed in background
# while working on the docs.
# Depends: inotify-tools

while inotifywait -e close_write $(find . | xargs) ; do
	make html
done
