# DjMetteo - Django Metteo Application

This is a django application that provides an MQTT daemon
to both parse data incoming from MySensors and display them.

## Setup

1. Clone this repositoy and cd to `djmetteo` directory

   ```bash
   git clone --recursive https://gitlab.com/rrbz/meteo/metteo
   cd metteo/djmetteo
   ```

   If you didn't clone recursively or want to update submodules:

   ```bash
   git submodule init
   git submodule update
   ```

2. (optional) setup a python virtualenvironment
   not to affect your system with djmetteo's
   requirements and activate it

   ```bash
   virtualenv --python=python3 env
   . env/bin/activate
   ```

3. Install required dependencies

   ```bash
   pip3 install -r requirements.txt
   ```

4. Migrate database

   ```bash
   python3 manage.py makemigrations djmetteo
   python3 manage.py migrate
   ```

5. Create a superuser

   ```bash
   python3 manage.py createsuperuser
   ```

You're now ready to run djmetteo.


## Working with djmetteo

You'll need to start both the MQTT daemon and the
server. Since they are both blocking, you'll need two
terminals (or run one in background).

In one you run

```bash
python3 manage.py runserver
```

so that you can connect to
[https://localhost:8000/admin](https://localhost:8000)
and login with the superuser you created before.

In the other one you run

```bash
python3 manage.py mqtt_daemon
```

Informations gathered from MQTT are printed
to the tty. You can refresh the admin via browser
to check if data is being stored in the database.
