# -*- coding: utf-8 -*-
# Copyright (c) 2020 Marco Marinello <me@marcomarinello.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Sensor(models.Model):
    mqtt_id = models.CharField(
        max_length=100,
        unique=True,
        verbose_name=_("MQTT sensor's id")
    )

    name = models.CharField(
        max_length=100,
        blank=True,
        null=True,
        verbose_name=_("sensor name")
    )

    kinds = [
        ["0/201/1", _("RPi Vin")],
        ["0/202/1", _("Console RSSI")],
        ["0/203/1", _("Gateway Temp.")],
        ["42/1/1", _("UV index")],
        ["42/2/1", _("SUN radiation")],
        ["42/3/1", _("Rain gauge")],
        ["42/4/1", _("Wind speed")],
        ["42/5/1", _("Wind direction")],
        ["42/6/1", _("Ambient temp.")],
        ["42/7/1", _("Ambient hum.")],
        ["42/8/1", _("Station temp.")],
        ["42/9/1", _("Air pressure")],
        ["42/10/1", _("Forecast")],
        ["42/201/1", _("Station batt.")],
        ["42/202/1", _("Station RSSI")],
        ["42/255/3/0/0", _("Station charge")]
    ]

    kind = models.CharField(
        choices=kinds,
        max_length=20,
        blank=True,
        null=True,
        verbose_name=_("sensor kind")
    )

    units = {
        "42/1/1": "UV Index",
        "42/2/1": "W/m^2",
        "42/3/1": "mm",
        "42/4/1": "km/h",
        "42/5/1": "",
        "42/6/1": "C",
        "42/7/1": "%",
        "42/8/1": "C",
        "42/9/1": "hPa",
        "42/10/1": "",
        "42/11/1": "",
        "42/201/1": "V",
        "42/202/1": "dBm",
        "42/203/1": "dBm",
    }

    def __str__(self):
        return "{} ({})".format(self.name, self.mqtt_id)

    def parse_kind(self):
        if not self.mqtt_id:
            return
        q = "/".join(self.mqtt_id.replace("mysensors-out/", "").split("/")[:3])
        if q in [a[0] for a in self.kinds]:
            self.kind = q
            self.name = self.get_kind_display()

    def unit(self):
        if self.kind and self.kind in self.units:
            return self.units[self.kind]

    def last_value(self):
        if self.measurement_set.exists():
            o = self.measurement_set.all()[0]
            return o.value_txt or o.value


    class Meta:
        verbose_name = _("sensor")
        verbose_name_plural = _("sensors")


class Measurement(models.Model):
    timestamp = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_("timestamp")
    )

    sensor = models.ForeignKey(
        'djmetteo.Sensor',
        on_delete=models.PROTECT,
        verbose_name=_("sensor")
    )

    value = models.FloatField(
        null=True,
        blank=True,
        verbose_name=_("float value")
    )

    value_txt = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("text value")
    )

    def __str__(self):
        return "{} {} {}".format(
            self.sensor,
            self.value or self.value_txt,
            self.timestamp
        )


    class Meta:
        ordering = ("-timestamp", "sensor")
        verbose_name = _("measurement")
        verbose_name_plural = _("measurements")
