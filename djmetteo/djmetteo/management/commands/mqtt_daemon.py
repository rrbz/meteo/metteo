# -*- coding: utf-8 -*-
# Copyright (c) 2020 Marco Marinello <me@marcomarinello.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings
from django.core.management.base import BaseCommand
from djmetteo.models import Sensor, Measurement
from paho.mqtt.client import Client


def parse_mqtt_incoming(x, y, z):
    # catch the sensor
    sensor = Sensor.objects.get_or_create(mqtt_id=z.topic)[0]
    if not sensor.kind:
        sensor.parse_kind()
        sensor.save()
    # try to parse the payload
    float_value = None
    text_value = None
    try:
        float_value = float(z.payload)
    except ValueError:
        try:
            text_value = z.payload.decode("utf-8")
        except:
            # wait: that's illegal
            # just a fallback not to lose the data
            text_value = "{}".format(z.payload)
    finally:
        Measurement.objects.create(
            sensor=sensor,
            value=float_value,
            value_txt=text_value
        )


class Command(BaseCommand):
    help = "Listens for data incoming from MQTT protocol and pushes this data" \
           "into the DB"

    def handle(self, *args, **kw):
        client = Client(client_id = "Metteo backend client")
        client.connect(settings.MQTT_SERVER)
        client.subscribe("#")
        client.on_message = parse_mqtt_incoming
        client.loop_forever()
