# -*- coding: utf-8 -*-
# Copyright (c) 2020 Marco Marinello <me@marcomarinello.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import csv

from django.conf import settings
from django.core.management.base import BaseCommand
from djmetteo.models import Sensor, Measurement


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("source", type=str)

    def handle(self, source, *args, **options):
        with open(source, newline="") as csvfile:
            reader = csv.reader(csvfile)
            for line in reader:
                if line[0].endswith("/43"):
                    continue
                sensor = Sensor.objects.get_or_create(mqtt_id=line[0].replace("mysensors-out/", ""))[0]
                if not sensor.kind:
                    sensor.parse_kind()
                    sensor.save()
                # try to parse the payload
                float_value = None
                text_value = None
                try:
                    float_value = float(line[1])
                except ValueError:
                    try:
                        text_value = line[1].decode("utf-8")
                    except:
                        # wait: that's illegal
                        # just a fallback not to lose the data
                        text_value = "{}".format(line[1])
                finally:
                    Measurement.objects.create(
                        sensor=sensor,
                        value=float_value,
                        value_txt=text_value
                    )

